\documentclass[12pt,a4paper]{article}
\usepackage[T1]{fontenc}
\usepackage{geometry}
\geometry{tmargin=1in,bmargin=1in,lmargin=1.2in,rmargin=1.2in}
\usepackage[english]{babel}
\usepackage{amsmath}
\usepackage{booktabs}
\usepackage{graphicx}
\usepackage{float}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{ae,aecompl}
\usepackage{authblk}
\usepackage[multiple]{footmisc}
\usepackage[hyperfootnotes=false]{hyperref}
\usepackage{bookmark}
\hypersetup{colorlinks=true,citecolor=blue}
\usepackage{amsthm}
\usepackage{MJOARTI}
\usepackage{setspace}
%\onehalfspacing
%\doublespacing
%\usepackage{vmargin}
\usepackage{graphicx}
\usepackage{rotating}
\usepackage{bbold}
\usepackage{lscape}
\usepackage{subfigure}
\usepackage{caption}
\usepackage{hyperref}
%\usepackage{floatrow}
\usepackage{lscape}
\usepackage{ifthen}
\usepackage{url}
\usepackage{blkarray}
\usepackage{multirow}
\usepackage{tabularx}
\usepackage{longtable}
\usepackage{enumerate}
\usepackage{supertabular}
\usepackage{fancyhdr}
\usepackage{epstopdf}
\usepackage{dsfont}
\usepackage{url}
\usepackage{multicol}
\usepackage{titlesec}
\usepackage{eurosym}
\usepackage{arydshln}
\usepackage{natbib}
\usepackage{threeparttable} % for notes below a table
\usepackage{comment}

\newcommand\citeapos[1]{\citeauthor{#1}'s (\citeyear{#1})}
\newtheorem{theorem}{Theorem}
\newtheorem{algorithm}{Algorithm}
\newtheorem{axiom}{Axiom}
\newtheorem{case}{Case}
\newtheorem{claim}{Claim}
\newtheorem{conclusion}{Conclusion}
\newtheorem{assumption}{Assumption}
\newtheorem{hypothesis}{Hypothesis}
\newtheorem{condition}{Condition}
\newtheorem{conjecture}{Conjecture}
\newtheorem{corollary}{Corollary}
\newtheorem{criterion}{Criterion}
\newtheorem{definition}{Definition}
\newtheorem{example}{Example}
\newtheorem{exercise}{Exercise}
\newtheorem{lemma}{Lemma}
\newtheorem{notation}{Notation}
\newtheorem{problem}{Problem}
\newtheorem{proposition}{Proposition}
\newtheorem{remark}{Remark}
\newtheorem{solution}{Solution}
\newtheorem{summary}{Summary}
\newtheorem{class}{Class}
\usepackage{array}
\newcolumntype{k}{>{\centering\arraybackslash}p{2cm}}
\usepackage{longtable}
\usepackage{ltcaption}
\usepackage{multicol}
\usepackage{color}
\usepackage{textcomp}
\definecolor{dark-red}{rgb}{0.6,0,0}
\definecolor{listinggray}{gray}{0.9}
\definecolor{darkgreen}{rgb}{0,0.4,0}
\definecolor{lbcolor}{rgb}{0.9,0.9,0.9}


%\setlength{\absleftindent}{-0.25in} % For Restart
%\setlength{\absrightindent}{-0.25in} % For Restart

%\renewcommand\thesection{\Roman{section}.}
%\renewcommand\thesubsection{\Alph{subsection}.}



\date{June 21, 2023}

\title{Do Thank-You Calls Increase Charitable Giving? Expert Forecasts and Field Experimental Evidence}
\author[1]{Jonathan Borowsky}
\author[2]{Grant Gibson (corresponding)}
\author[3]{Audrée Langlois}
\author[4]{Rosalie Montambeault}
\author[5]{Moyo Sogaolu}
\affil[1]{University of Minnesota; CFANS}
\affil[2]{McMaster University; CRDCN}
\affil[3]{Laval University}
\affil[4]{Laval University}
\affil[5]{McMaster University}
\begin{document}
\maketitle
%\thanks{Authors: Brodeur: University of Ottawa and IZA. E-mail: \href{mailto:abrodeur@uottawa.ca}%{abrodeur@uottawa.ca}. }
\onehalfspacing
\begin{abstract}

\small{\cite{SAL} estimate the effect of 'thank you calls' on the extensive and intensive margins of subsequent donations. Based on a series of experimental interventions, the authors find no statistically discernable effect of thank-you calls on either the likelihood of donating again, or on the size of any subsequent donations made within the period of the study. In a companion exercise the researchers quantify the ability of experts in charitable fundraising and non-experts (using the Understanding America Survey) to predict the behaviours elicited by the experiment. Experts and non-experts (incorrectly) make the same predictions of an increase to the extensive margin of donation behaviour induced by the thank you call, and while both groups overestimate the intensive margin, the non-experts overestimated by a smaller magnitude. We were able to reproduce the papers findings completely, discovering only one difference in an appendix table related to the average gift amount | treatment for experiment 1 where only the constant term of the regression was affected. Upon careful examination of the code we found a few small errors that did not affect the results (one of the errors in the code did not seem to be carried through and used anywhere). Finally, we conducted several extensions of the original analysis which demonstrated that the findings are robust to heterogeneity of treatment effect by initial donation size, as well as different specifications of the regression analysis.}  


%\textsc{Keywords}:  \newline

\textsc{JEL codes}: C83; C93; D64; D91; L31; L82.
\end{abstract}

\clearpage
\doublespacing

\section{Introduction}

In their AEJ: applied article \textit {Do Thank-You Calls Increase Charitable Giving? Expert Forecasts and Field Experimental Evidence} \cite{SAL} investigated the effects of thank you calls on the probability that a donor who was thanked would donate again, and the amount donated conditional on donating. Across three different experiments, where the calls were explicitly not aiming to induce a new donation, but merely to say "thank you", the authors find zero statistically distinguishable intensive or extensive margin effects of the thank you call. Donation amount differed by less than 30 cents across the larger experiments and the percent donating by less than 1/10 of a percentage point. 

Computational reproducibility was verified (with the exception of a single constant term in a regression in the appendix - see table 1), we also conducted several robustness checks related to the functional form of the analysis, and extended the analysis to investigate whether the treatment effect of the thank you call was nonzero for donors who made larger donations prior to the experiment.

Finally, we thoroughly examined the code (there was too much code in this project to recode the entire analysis). We found a few small errors that did not affect the results in a way that would overturn or modify the results from the paper. The first was an error in coding the dates, the second was a mistake in the treatment of individuals who made more than one donation. Neither of these affected the magnitude or significance of any of the coefficients, or any conclusions that should be drawn from the experiments.

\section{Reproducibility}

We describe in this section two minor coding errors that we uncovered while reproducing the study. Most critically, there was an error related to individuals who made more than one donation in the pre-intervention period. For this, where there are dummy variables for the kind of gift, technique etc. these were treated with the mean function. Where it makes sense to sum up the gift amount for a given day, the same can't be said for applying a mean function to categorical information related to the gift. For our recode, in the absence of a compelling reason to choose otherwise, we recoded to choose the minimum value of the category for the day. This recode resulted in no change to experiment 2, but an additional 95 observations in the treatment group (out of over 430,000 observations), and seven in the control (out of over 54,000) for experiment 1. In experiment 3 the addition to the samples were 2 observations for the "new script", 3 observations for the "original script", and 1 for the control group (over samples of thousands). The changes to the averages and treatment effects as a result of these miniscule changes to the sample do not change the results of the study. Finally, the data provided by the Understanding America Study was not in .dta format although the code tried to call it as if it were. We re-saved the data download in .dta format to resolve this issue.

Tables 2 \& 3 shows the revised treatment and control figures based on this fix to the code.

There were several other coding errors in the files one related to the coding of a date variable which was formatted incorrectly (the code transposed day and month in the specification of the date format). This would result in a date variable that would be missing for dates with a value of day greater than 12 and incorrect values for other days. The next is a figure that did not have the correct calculation to create error bars (specifying error bars that are +/- 5\% of the value of the mean). Both these errors when corrected did not change the output which suggests that they ultimately were not used. The scale of the project made it difficult to track whether the date error had been used anywhere in the subsequent code (though the lack of change in any output for the scale of the fix that the correct date code represents provides nearly incontrovertible evidence that it was not), the figure with the mis-specified error bars was clearly never used in the output, but was simply never deleted.

\section{Replication}

Our replication exercises exclusively focused on experiment 1. We tested whether there was heterogeneity in the treatment effect, and whether the functional form assumption of the model mattered to the result. We investigated whether adding institutional fixed effects for the different stations in experiment 1 mattered for either the percent donating, or amount of donation (which would have a non-zero effect if randomization across institution were not perfect), we also examined whether the structure of the model mattered by allowing the number of donations to be modelled as a Poisson regression. We did not pre-register these analyses, but each author came to the replication exercise having conceived of one extension based on their read of the paper. For comparison, we include an OLS regression to serve as a proxy for the mean-comparison that is conducted in the original analysis. The only change to findings is that if the number of donations is modelled as a poisson process, the qualitative conclusion (that the treatment group make fewer donations on average) remains, but the result becomes statistically significant - althought the point estimate and 95\% confidence interval remain very close to 1.0 which would represent parity.

\subsection{Fixed effects}

The following table presents the results of introducing fixed effects to the probability of donating (Panel A), and the amount donated and number of gifts (Panel B). The data about the pre-treatment randomization across radio stations was not presented (though the implication was that it would be balanced). A change here (i.e. a difference between treated and controlled) would have meant that this piece of the randomization failed. 

\subsection{Heterogeneous treatment effects}

In addition to the fixed effects, we also examined whether there was heterogeneity in the treatment effects. The question was whether a thank you call was appreciated by large donors (i.e. a positive treatment effect at the top of the pre-treatment distribution of donations), but is washed out in the aggregate by a large number of smaller donors who don't care about the phone call. Whether the treatment effect was different for those in the top half of the pre-treatment donation, or the top 10\% of the pre-treatment donation was investigated by running three separate models\footnote{We also tried specifying this as a pooled model, but this didn't change the result}, one restricting the sample to those whose initial donation was below the median, one restricted to those above the median, and lastly those in the top 10\%. The statistical effects were similar to what had been observed before, we note that the "number of gifts" result in the poisson model is being mostly driven by those who donate more.

\section{Conclusion}

To conclude, the results from the analysis were computationally reproducible, and robust to several extensions. The minor errors discovered in the code in no way overturned findings as presented in the original study. Though there was a lot of unused lines of code in the material provided, the research was reproducible with almost no issues.

\newpage
\bibliographystyle{kluwer}
\bibliography{biblio}

\newpage

\newpage
\section{Tables}

\begin{table}[H]
\centering
\caption{Computational reproducibility check}  \label{tab:table1}
\begin{threeparttable}
 \footnotesize 
\begin{tabular}{l|r|r}
\toprule
& Experiment 1 (article version) & Experiment 1 (reproducer’s version) \\
\hline
Reached & 0.0 (0.00) & 0.0 (0.00)\\
New Call Script & [blank] & [blank] \\
Baseline Gift Amount & -0.0 (0.00)& -0.0 (0.00)\\
Baseline Number of Gift & 0.13 (0.00) & 0.13 (0.00)\\
Female & 0.01 (0.00) & 0.01 (0.00)\\
45-64 & 0.00 (0.00)& 0.00 (0.00)\\
65+ years old & 0.08 (0.00)& 0.08 (0.00)\\
Income \$35,000-\$99,999 & 0.01 (0.00)& 0.01 (0.00)\\
Income \$100,000-\$174,999 & 0.03 (0.00)& 0.03 (0.00)\\
Income \$175,000+ & 0.04 (0.00)& 0.04 (0.00)\\
Residence length>5yrs & 0.00 (0.00)& 0.00 (0.00)\\
Constant & 0.43 (0.32) & -0.18 (0.86) \\
\bottomrule
\end{tabular}
\begin{tablenotes}
	\item Standard errors in parentheses
	\item This is table A2 in the original article
	\item We hypothesized that this difference in the constant term may have been caused by a change in the reference charity for this model (which used fixed effects), this could have been caused by differences in stata version.
	\item This table represents results from running the unadulterated code as provided by the authors
\end{tablenotes}
\end{threeparttable}     
\end{table}         


\begin{table}[H]
   \centering
   \caption{Revised Panel A} \label{tab:table2}
   \begin{threeparttable}
         \footnotesize 

\begin{tabular}{l|rr|rr}
Variable         & Treatment & Treatment (recoded) & Control & Control (recoded) \\ \hline
Percent donating & 27.97     & 27.98               & 28.06   & 28.07             \\
Amount donated   & 35.52     & 35.55               & 35.49   & 35.51             \\
Number of gifts  & 0.79      & 0.79                & 0.80    & 0.80              \\
Amount|Donated   & 127.00    & 127.05              & 126.46  & 126.48            \\
Retention rate   & 32.68     & 32.70               & 32.91   & 32.92             \\
Observations     & 439,510   & 439,605             & 54,606  & 54,613           
\end{tabular} 
        \begin{tablenotes}
	\item This is Panel A from Table 2 in the original paper
	\item Note that Panel B was not revised because the recode did not modify any values
	\item The min function results in fewer missing values to categorical information (and thus a larger sample)
        \end{tablenotes}  
   \end{threeparttable}                          
\end{table}

\begin{table}[H]
   \centering
   \caption{Revised Panel C} \label{tab:table3}
   \begin{threeparttable}
         \footnotesize 
\begin{tabular}{l|rr|rr|rr}
Variable         & Treatment New script & Treatment New script (recoded) & Treatment Original script & Treatment Original script (recoded) & Control & Control (recoded) \\ \hline
Percent donating & 28.32                & 28.32                          & 29.15                     & 29.17                               & 27.01   & 27.04             \\
Amount donated   & 34.41                & 34.42                          & 34.64                     & 34.67                               & 30.88   & 30.90             \\
Number of gifts  & 0.92                 & 0.92                           & 0.98                      & 0.98                                & 0.90    & 0.91              \\
Amount|Donated   & 121.49               & 121.52                         & 118.84                    & 118.87                              & 114.33  & 114.31            \\
Retention rate   & 39.00                & 39.02                          & 40.72                     & 40.82                               & 37.69   & 37.73             \\
Observations     & 10,780               & 10,782                         & 10,893                    & 10,896                              & 2,640   & 2,641            
\end{tabular} 
        \begin{tablenotes}
	\item This is Panel C from Table 2 in the original paper
	\item The min function results in fewer missing values to categorical information (and thus a larger sample)
        \end{tablenotes}  
   \end{threeparttable}                          
\end{table}


\begin{table}[H]
   \centering
   \caption{Extension - Fixed effects: Percent donating \& retention} \label{tab:table4}
   \begin{threeparttable}
	\footnotesize
\begin{tabular}{l|rr}
Outcome          & OLS diff  & OLS diff with Station FE   \\ \hline
Percent donating & -0.001 (0.655) & 0.000 (0.854)      \\ 
Retention   & -0.002 (0.568) & -0.000 (0.954)
\end{tabular}       
        \begin{tablenotes}
	\item The OLS difference column is meant to proxy the difference in the means as presented in the original paper for ease of comparison.
        	\item p-values in parentheses
        \end{tablenotes}  
   \end{threeparttable}                          
\end{table}
\begin{table}[H]
   \centering
   \caption{Extension - Fixed effects and Poisson process: Amount of donations \& number of gifts} \label{tab:table5}
   \begin{threeparttable}
\footnotesize 
\begin{tabular}{l|rrr}
\hline
Outcome          & OLS diff  & OLS diff with Station FE & Poisson Regression (IRR) \\ \hline
Gift amount & 0.045 (0.916) & 0.143 (0.736) & -     \\ 
Number of gifts   & -0.012 (0.213) & -0.008 (0.440) & 0.985 (0.003) \\
Gift amount | giving &  0.572 (0.636) &  0.236 (0.844) & -
\end{tabular}    
         \begin{tablenotes}
     	\item The OLS difference column is meant to proxy the difference in the means as presented in the original paper for ease of comparison
	\item p-values in parentheses
	\end{tablenotes}  
   \end{threeparttable}                          
\end{table}

\begin{table}[H]
   \centering
   \caption{Extension - Heterogenous treatment effects} \label{tab:table6}
   \begin{threeparttable}
  \footnotesize 
\begin{tabular}{l|rrr}
\hline
         & below median  & above median  & above 90th percentile \\ \hline
Gift amount & -0.216 (0.652) & 0.325 (0.646) & 0.653 (0.793)     \\ 
Number of gifts (IRR)   & 998 (0.680) & 0.962** (0.000) & 0.965 (0.073)  \\
Gift amount | giving &  0.122 (0.902) &  0.220 (0.939) & -1.28 (0.894)
\end{tabular}    
\begin{tablenotes}
\item p-values in parentheses
        \end{tablenotes}  
      \end{threeparttable}                          
\end{table}


\end{document} 
