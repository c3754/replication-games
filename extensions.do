//File to create the extensions in the reproducibility exercise from the I4R

//Regressions of extensions - the coefficients on "treat" are what appear in the tables in the reproduction and represent the treatment effect in $. In the original paper table 2 the reader compares the averages between treatment and control rather than the difference (which we present here). The value of the difference is not the same as that which would be obtained by subtracting the values from the paper's table 2 because we here use the code correction for those donating multiple times in one day.


log using "$dir/replication extension/resultlog", replace

//Generate a few variables needed for the extensions.
//create a station id


cd "$dir/data"
use exp1_analysis_data, clear
encode station, gen(stationid)
xtset stationid


//First table (T4) - retention and percent donating
//unadjusted mean difference
reg renewing treat
reg retention treat

//include FE
xtreg renewing treat,fe
xtreg retention treat,fe


//Second table (T5)

**AMOUNT DONATED**
//unadjusted mean difference
reg payment_amount3 treat

//station fixed effect
xtreg payment_amount3 treat, fe


**NUMBER OF GIFTS**
//unadjusted mean difference
reg var13 treat

//station fixed effect
xtreg var13 treat, fe

//poisson distribution
poisson var13 treat, irr

**AMOUNT DONATED | GIFT MADE**
//unadjusted mean difference
reg payment_amount3 treat if payment_amount3>0

//station fixed effect
xtreg payment_amount3 treat if payment_amount3>0, fe


//Heterogenous treatment effects based on baseline gift amount
summarize payment_amount2, d

gen pt50=(payment_amount2>r(p50))
gen pt10=(payment_amount2>r(p90))


	reg payment_amount3 treat if pt50==0
	reg payment_amount3 treat if pt50==1
	reg payment_amount3 treat if pt10==1
	
	
	poisson var13 treat if pt50==0, irr
	poisson var13 treat if pt50==1, irr
	poisson var13 treat if pt10==1, irr
	
	reg payment_amount3 treat if payment_amount3>0 & pt50==0
	reg payment_amount3 treat if payment_amount3>0 & pt50==1
	reg payment_amount3 treat if payment_amount3>0 & pt10==1