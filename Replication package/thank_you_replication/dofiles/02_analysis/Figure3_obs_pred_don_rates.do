********************************************************************************
*Purpose: This do file creates Figure 3
*Input Files: charity_round2.dta
*Output Files: figure3_obs_pred_don_rates.pdf
********************************************************************************


cd "$dir/data"
use "charity_round2", clear

*creating globals 

global donation_rates mail_1 mail_2 door_1 door_2 door_3 matching_1 matching_2 matching_3 volunteer_1 volunteer_2 agency_1 agency_3
global cond_amounts mail_3 mail_4 volunteer_3 volunteer_4 calling_1 calling_2 calling_3 matching_4 matching_5 matching_6 agency_2 agency_4

*Generating Predicted and Observed Effects

foreach var of varlist $cond_amounts $donation_rates {
	gen peffect_`var'=`var' - `var'_control
	gen aeffect_`var'=`var'_actual - `var'_control
	gen sepeffect_`var'=`var' - `var'_control
}


*gen se for actual obsv, donation rates
foreach var of varlist $donation_rates {
summarize `var'_actual
local success1=r(mean)/100
summarize `var'_control
local success2=r(mean)/100
di `success1'
di `success2'
di (`success1'*(1-`success1'))*(`success2'^2)
gen seaeffect_`var'=sqrt(((`success1'*(1-`success1'))/`var'_nactual) + ((`success2'*(1-`success2'))/`var'_ncontrol))
replace seaeffect_`var'=seaeffect_`var'*100

}



*gen se for actual obsv, cond amounts
foreach var of varlist $cond_amounts {
gen seaeffect_`var'=sqrt((`var'_seactual)^2 + (`var'_secontrol)^2)
}


foreach var of varlist $donation_rates $cond_amounts {
	gen treatment_`var'=`var'_actual
}


collapse (mean) aeffect_* peffect_* seaeffect_* treatment_* (semean) sepeffect_* 


local count=1
foreach var in $donation_rates $cond_amounts {
	rename peffect_`var' peffect`count'
	rename sepeffect_`var' sepeffect`count'
	rename aeffect_`var' aeffect`count'
	rename seaeffect_`var' seaeffect`count'
	rename treatment_`var' treatment`count'
	local count=`count'+1
}

*reshaping for graph 

gen id=1
reshape long peffect aeffect sepeffect seaeffect treatment control forecast/*
*/ setreatment secontrol seforecast, j(experiment) i(id)
drop id

*generating CI

gen hi_p = peffect + 1.96*sepeffect
gen lo_p = peffect - 1.96*sepeffect
gen hi_a = aeffect + 1.96*seaeffect
gen lo_a = aeffect - 1.96*seaeffect



gen y=0

keep if experiment<=12 & experiment!=3 & experiment!=5 & experiment!=10 & experiment!=9
replace experiment=experiment-3 if experiment>=6 & experiment<=8
replace experiment=6 if experiment==4 & treatment==5
replace experiment=experiment-4 if experiment>=11

label define experiment4 1 "(Falk,2007) 1 Gift" 2 "(Falk,2007) 4 Gifts" /*
	*/ 3 "(Karlan & List, 2007) 1:1 Match" 4 "(Karlan & List, 2007) 2:1 Match" 5 "(Karlan & List, 2007) 3:1 Match" 6 "(DellaVigna et al.,2012) Opt-out" 7  "(Kessler et al., 2019) Overall" 8 "(Kessler et al., 2019) Rich & Powerful"

	
	label values experiment experiment4

twoway (scatter peffect experiment, msize(small) mfcolor(black) mlcolor(black) )/*
*/ (scatter aeffect experiment, msize(small) mfcolor(none) mlcolor(gray)) /*
*/ (rcap hi_p lo_p experiment, lcolor(black)) (rcap hi_a lo_a experiment, lcolor(gray)) (line y experiment, lpattern(dash)),/*
*/ ytitle("Change in % Donated") xtitle("") legend(order(1 "Predicted Effect" 2 "Observed Effect"))/*
*/ graphregion(color(white)) xlabel(#8,valuelabel labsize(vsmall) angle(25))

cd "$dir/latex"
graph export figure3_obs_pred_don_rates.pdf, replace

