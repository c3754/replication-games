********************************************************************************
*Purpose: This do file creates Table 1
*Input Files: exp1_analysis_data, exp2_analysis_data, exp3_analysis_data
*Output Files: table1_balance_table.tex
********************************************************************************


cd "$dir/data"
use exp1_analysis_data, clear
	
foreach var of varlist var12 payment_amount2 {
ranksum `var', by(treat) porder
*Mann Whitney doesn't save p-value, so have to calculate from z with 2 * normprob(-abs(r(z)))
local `var'_p = 2 * normprob(-abs(r(z)))
summarize `var' if treat==0
local `var'_cntrl_mean = r(mean)
local `var'_cntrl_se= (r(sd)/(sqrt(r(N))))
summarize `var' if treat==1
local `var'_treat_mean = r(mean)
local `var'_treat_se=(r(sd)/(sqrt(r(N))))
}

foreach var of varlist female age_display* inc_display* lor_display* female_missing age_income_missing {
tabulate `var' treat, chi2
local `var'_p = r(p)
summarize `var' if treat==0
local `var'_cntrl_mean = r(mean)*100
local `var'_cntrl_se= (r(sd)/(sqrt(r(N))))*100
summarize `var' if treat==1
local `var'_treat_mean = r(mean)*100
local `var'_treat_se=(r(sd)/(sqrt(r(N))))*100
}

foreach var of varlist var12 payment_amount2 female age_display* inc_display* lor_display* female_missing age_income_missing {
*add significance stars
if ``var'_p'>.01 {
	local `var'_star=""
			}
if ``var'_p' <= .1 & ``var'_p' > .05 {
		local `var'_star="*"
		}
	if ``var'_p' <= .05 & ``var'_p' > .01 {
		local `var'_star="**"
		}
	if ``var'_p' <= .01 {
		local `var'_star="***"
		}
}

label var left_message "\%Left Message"
		label var positive_engagement "\%Positive Engagement"
		label var negative_engagement "\%Negative Engagement"
		label var neutral_engagement "\%Neutral Engagement"
		
foreach var of varlist reached left_message positive_engagement neutral_engagement negative_engagement {
summarize `var' 
				local `var'_treat_mean = r(mean)*100
				local `var'_treat_se=(r(sd)/(sqrt(r(N))))*100
}
label var reached "\% Reached"


count if treat==1
local num_treat_mean = r(N)
gen num = r(N)
label var num "N"
count if treat==0
local num_cntrl_mean = r(N)

cd "$dir/latex"
file open fh using "table1_balance_table.tex", write replace
		file write fh "\begin{subtable}[t]{\linewidth}" _n ///
			"\centering" _n ///
			"\vspace{0pt}" _n ///
			"\caption{Experiment 1: Public Television Stations}" _n ///
			"\begin{tabular}{l c c c}" _n ///
			"& \multicolumn{1}{c}{Treatment} & \multicolumn{1}{c}{Control} & \multicolumn{1}{c}{p-value}\\" _n ///
			"\hline" _n ///
			"\hline" _n 

	
foreach var of varlist payment_amount2 var12 female age_display* inc_display4/*
*/ inc_display3 inc_display1 inc_display2  {
			
			


			file write fh "\hspace{4mm}"`"`: var label `var''"'" & " %4.2f (``var'_treat_mean') " & " %4.2f (``var'_cntrl_mean') " & " %4.2f (``var'_p') "``var'_star' \\" _n 
			file write fh " &  (" %4.2f (``var'_treat_se') ") & (" %4.2f (``var'_cntrl_se') ") & \\" _n  
			
			}
			
			foreach var of varlist reached left_message positive_engagement neutral_engagement negative_engagement num {
				
				if `var'== num {
			file write fh "\hline" _n
			file write fh "\hspace{4mm}"`"`: var label `var''"'" & " %15.0fc (``var'_treat_mean') " & " %15.0fc (``var'_cntrl_mean') " & \\" _n 
			}
			
			else{
			file write fh "\hspace{4mm}"`"`: var label `var''"'" & " %4.2f (``var'_treat_mean') " & & \\" _n 
			file write fh " &  (" %4.2f (``var'_treat_se') ") & & \\" _n  
			}
			}
			
file write fh "\hline" _n ///
			  "\hline" _n ///
			  "\vspace{0pt}" _n
file write fh "\end{tabular}" _n

*Experiment 2
*-----------------------
clear
cd "$dir/data"
use exp2_analysis_data, replace 


foreach var of varlist payment_amount2 var12 {
ranksum `var', by(treat) porder
local `var'_p = 2 * normprob(-abs(r(z)))
summarize `var' if treat==0
local `var'_cntrl_mean = r(mean)
local `var'_cntrl_se= (r(sd)/(sqrt(r(N))))
summarize `var' if treat==1
local `var'_treat_mean = r(mean)
local `var'_treat_se=(r(sd)/(sqrt(r(N))))
}

foreach var of varlist renewing {
tabulate `var' treat, chi2
local `var'_p = r(p)
summarize `var' if treat==0
local `var'_cntrl_mean = r(mean)*100
local `var'_cntrl_se= (r(sd)/(sqrt(r(N))))*100
summarize `var' if treat==1
local `var'_treat_mean = r(mean)*100
local `var'_treat_se=(r(sd)/(sqrt(r(N))))*100
}

foreach var of varlist var12 payment_amount2 renewing {
*add significance stars
if ``var'_p'>.1 {
	local `var'_star=""
			}
if ``var'_p' <= .1 & ``var'_p' > .05 {
		local `var'_star="*"
		}
	if ``var'_p' <= .05 & ``var'_p' > .01 {
		local `var'_star="**"
		}
	if ``var'_p' <= .01 {
		local `var'_star="***"
		}
}

count if treat==1
local num_treat_mean = r(N)
gen num = r(N)
label var num "N"
count if treat==0
local num_cntrl_mean = r(N)

gen reached = 1 if response_type == "Called: Contacted"
		replace reached=0 if response_type=="Called: Not Contacted"
		foreach var of varlist reached  {
				summarize `var' 
				local `var'_treat_mean = r(mean)*100
				local `var'_treat_se=(r(sd)/(sqrt(r(N))))*100
		}
						label var reached "\% Reached"

						file write fh "\caption{Experiment 2: National Non-Profit}" _n ///
			"\vspace{0pt}" _n ///
			"\begin{tabular}{l c c c}" _n ///
			"& \multicolumn{1}{c}{Treatment} & \multicolumn{1}{c}{Control} & \multicolumn{1}{c}{p-value}\\" _n ///
			"\hline" _n ///
			"\hline" _n 
			
foreach var of varlist payment_amount2 var12  {

		

			file write fh "\hspace{4mm}"`"`: var label `var''"'" & " %4.2f (``var'_treat_mean') " & " %4.2f (``var'_cntrl_mean') " & " %4.2f (``var'_p') "``var'_star' \\" _n 
			file write fh " &  (" %4.2f (``var'_treat_se') ") & (" %4.2f (``var'_cntrl_se') ") & \\" _n  
			
			}
			
			
			foreach var of varlist reached num{
			
				if `var'== num {
			file write fh "\hline" _n
			file write fh "\hspace{4mm}"`"`: var label `var''"'" & " %15.0fc (``var'_treat_mean') " & " %15.0fc (``var'_cntrl_mean') " & \\" _n 
			}
			
			else{
				file write fh "\hspace{4mm}"`"`: var label `var''"'" & " %4.2f (``var'_treat_mean') " \\" _n 
			file write fh " &  (" %4.2f (``var'_treat_se') ") \\" _n  
			}
			}
		

		file write fh	"\hline" _n ///
			"\hline" _n ///
			"\vspace{0pt}" _n
file write fh "\end{tabular}" _n
file write fh "\end{subtable}" _n
file write fh "\end{table}" _n
file write fh "\pagebreak" _n
file write fh "\begin{table}[t]" _n
file write fh "\ContinuedFloat" _n
file write fh "\caption{Balance Table, cont.}" _n
file write fh "\begin{subtable}[t]{\linewidth}" _n

*Experiment 3
clear
cd "$dir/data"
use exp3_analysis_data, replace

file write fh "\caption{Experiment 3: Public Television, New Script}" _n ///
			"\vspace{0pt}" _n ///
			"\begin{tabular}{l c c c c}" _n ///
			"& \multicolumn{1}{c}{New Script} & \multicolumn{1}{c}{Original Script} & \multicolumn{1}{c}{Control} & \multicolumn{1}{c}{p-value}\\" _n ///
			"\hline" _n ///
			"\hline" _n 

	
foreach var of varlist var12 payment_amount2 female age_display* inc_display* /*
*/lor_display* female_missing age_income_missing{
oneway `var' exp3_treat 
local `var'_p = string(Ftail(r(df_m), r(df_r), r(F)), "%9.2f")

if `var'==var12 | `var'==payment_amount2 {
local scalar=1 
}
else {
local scalar=100
}
summarize `var' if exp3_treat==0
local `var'_cntrl_mean = r(mean)*`scalar'
local `var'_cntrl_se= (r(sd)/(sqrt(r(N))))*`scalar'
summarize `var' if exp3_treat==1
local `var'_treat1_mean = r(mean)*`scalar'
local `var'_treat1_se=(r(sd)/(sqrt(r(N))))*`scalar'
summarize `var' if exp3_treat==2
local `var'_treat2_mean = r(mean)*`scalar'
local `var'_treat2_se=(r(sd)/(sqrt(r(N))))*`scalar'

*add significance stars
if ``var'_p'>.1 {
	local `var'_star=""
			}
if ``var'_p' <= .1 & ``var'_p' > .05 {
		local `var'_star="*"
		}
	if ``var'_p' <= .05 & ``var'_p' > .01 {
		local `var'_star="**"
		}
	if ``var'_p' <= .01 {
		local `var'_star="***"
		}
}

count if exp3_treat==1
local num_treat1_mean = r(N)
gen num = r(N)
label var num "N"
count if treat==0
local num_cntrl_mean = r(N)
count if exp3_treat==2
local num_treat2_mean = r(N)

label var left_message "\%Left Message"
		label var positive_engagement "\%Positive Engagement"
		label var negative_engagement "\%Negative Engagement"
		label var neutral_engagement "\%Neutral Engagement"

		foreach var of varlist reached left_message positive_engagement neutral_engagement negative_engagement female age_display* inc_display* {
			    summarize `var' if exp3_treat==0
				local `var'_cntrl_mean = r(mean)*100
				local `var'_cntrl_se=(r(sd)/(sqrt(r(N))))*100
				
				summarize `var' if exp3_treat==1
				local `var'_treat1_mean = r(mean)*100
				local `var'_treat1_se=(r(sd)/(sqrt(r(N))))*100
				summarize `var' if exp3_treat==2
				local `var'_treat2_mean = r(mean)*100
				local `var'_treat2_se=(r(sd)/(sqrt(r(N))))*100
				}
		
			label var reached "\% Reached"

			
foreach var of varlist payment_amount2 var12 female age_display* inc_display4/*
*/ inc_display3 inc_display1 inc_display2   {
			
			file write fh "\hspace{4mm}"`"`: var label `var''"'" & " %4.2f (``var'_treat2_mean') " & " %4.2f (``var'_treat1_mean') " & " %4.2f (``var'_cntrl_mean') " & " %4.2f (``var'_p') "``var'_star' \\" _n 
			file write fh " &  (" %4.3f (``var'_treat2_se') ") & (" %4.2f (``var'_treat1_se') ") & (" %4.2f (``var'_cntrl_se') ") & \\" _n  
		
			}
			
foreach var of varlist reached left_message positive_engagement neutral_engagement negative_engagement num {
	
	if `var'== num {
			file write fh "\hline" _n
			file write fh "\hspace{4mm}"`"`: var label `var''"'" & " %15.0fc (``var'_treat2_mean') " & " %15.0fc (``var'_treat1_mean') " & " %15.0fc (``var'_cntrl_mean') " & \\" _n 
			}
			
			else {
				file write fh "\hspace{4mm}"`"`: var label `var''"'" & " %4.2f (``var'_treat2_mean') " & " %4.2f (``var'_treat1_mean') " \\" _n 
			file write fh " &  (" %4.2f (``var'_treat2_se') ") & (" %4.2f (``var'_treat1_se') ")  \\" _n  
		 }
}
				
			
			
file write fh "\hline" _n ///
			"\hline" _n 
file write fh "\end{tabular}" _n
file write fh "\end{subtable}" _n

file close fh 
