********************************************************************************
*Purpose: This do file creates Table 2
*Input Files: exp1_analysis_data, exp2_analysis_data, exp3_analysis_data
*Output Files: table2_treatment_effects.tex
********************************************************************************
//ON
pause on


cd "F:/replication-games/Replication package/thank_you_replication"


foreach experiment in 1 2 3 {
clear
cd "$dir/data"
use exp`experiment'_analysis_data

if `experiment'==1 {
cd "$dir/latex"
file open fh using "table2_treatment_effects.tex", write replace

		file write fh "\begin{subtable}[t]{\linewidth}" _n ///
			"\centering" _n ///
			"\vspace{0pt}" _n ///
			"\caption{Experiment 1: Public Television Stations}" _n ///
			"\begin{tabular}{l c c c c}" _n ///
			"& \multicolumn{1}{c}{Treatment} & \multicolumn{1}{c}{Control} & \multicolumn{1}{c}{p-value} & \multicolumn{1}{c}{}\\" _n ///
			"\hline" _n ///
			"\hline" _n 
}

if `experiment'==2 {
	file write fh "\caption{Experiment 2: National Non-Profit}" _n ///
			"\vspace{0pt}" _n ///
			"\begin{tabular}{l c c c c}" _n ///
			"& \multicolumn{1}{c}{Treatment} & \multicolumn{1}{c}{Control} & \multicolumn{1}{c}{p-value} & \multicolumn{1}{c}{}\\" _n ///
			"\hline" _n ///
			"\hline" _n 
			}

if `experiment'==3 {
file write fh "\caption{Experiment 3: Public Television, New Script}" _n ///
			"\vspace{0pt}" _n ///
			"\begin{tabular}{l c c c c}" _n ///
			"& \multicolumn{1}{c}{New Script} & \multicolumn{1}{c}{Original Script} & \multicolumn{1}{c}{Control} & \multicolumn{1}{c}{p-value}\\" _n ///
			"\hline" _n ///
			"\hline" _n 
}


foreach var of varlist payment_amount3 var13 retention renewing {

if `experiment'==3 {
*Unconditional Gift, includes 0 for no renewers
oneway `var' exp3_treat 
local `var'_p = string(Ftail(r(df_m), r(df_r), r(F)), "%9.2f")
summarize `var' if exp3_treat==0

if "`var'" =="renewing" | "`var'"== "retention" {
local `var'_cntrl_mean = r(mean) * 100 
local `var'_cntrl_se= (r(sd)/(sqrt(r(N)))) * 100
summarize `var' if exp3_treat==1
local `var'_treat1_mean = r(mean) * 100
local `var'_treat1_se=(r(sd)/(sqrt(r(N)))) * 100
summarize `var' if exp3_treat==2
local `var'_treat2_mean = r(mean) * 100
local `var'_treat2_se=(r(sd)/(sqrt(r(N)))) * 100
}
else {
local `var'_cntrl_mean = r(mean)
local `var'_cntrl_se= (r(sd)/(sqrt(r(N))))
summarize `var' if exp3_treat==1
local `var'_treat1_mean = r(mean)
local `var'_treat1_se=(r(sd)/(sqrt(r(N))))
summarize `var' if exp3_treat==2
local `var'_treat2_mean = r(mean)
local `var'_treat2_se=(r(sd)/(sqrt(r(N))))
}

*Conditional on Donating aka renewing==1
oneway `var' exp3_treat if renewing==1
local `var'_p_con = string(Ftail(r(df_m), r(df_r), r(F)), "%9.2f")
summarize `var' if exp3_treat==0 & renewing==1
local `var'_cntrl_mean_con = r(mean)
local `var'_cntrl_se_con = (r(sd)/(sqrt(r(N))))
summarize `var' if exp3_treat==1 & renewing==1
local `var'_treat1_mean_con = r(mean)
local `var'_treat1_se_con=(r(sd)/(sqrt(r(N))))
summarize `var' if exp3_treat==2 & renewing==1
local `var'_treat2_mean_con = r(mean)
local `var'_treat2_se_con =(r(sd)/(sqrt(r(N))))
}

else {
*Unconditional Gift, includes 0 for no renewers
if "`var'" == "renewing" {
tabulate `var' treat, chi2
local `var'_p = r(p)
}
else {
ranksum `var', by(treat) porder
local `var'_p = 2 * normprob(-abs(r(z)))
}

if "`var'" =="renewing" | "`var'"== "retention" {
summarize `var' if treat==0
local `var'_cntrl_mean = r(mean) * 100
local `var'_cntrl_se= (r(sd)/(sqrt(r(N)))) * 100
summarize `var' if treat==1
local `var'_treat_mean = r(mean) * 100
local `var'_treat_se=(r(sd)/(sqrt(r(N)))) * 100
}

else {
summarize `var' if treat==0
local `var'_cntrl_mean = r(mean) 
local `var'_cntrl_se= (r(sd)/(sqrt(r(N))))
summarize `var' if treat==1
local `var'_treat_mean = r(mean)
local `var'_treat_se=(r(sd)/(sqrt(r(N))))
}

*Conditional on Donating aka renewing==1
ranksum `var' if renewing==1, by(treat) porder
local `var'_p_con = 2 * normprob(-abs(r(z)))
summarize `var' if treat==0 & renewing==1
local `var'_cntrl_mean_con = r(mean)
local `var'_cntrl_se_con = (r(sd)/(sqrt(r(N))))
summarize `var' if treat==1 & renewing==1
local `var'_treat_mean_con = r(mean)
local `var'_treat_se_con =(r(sd)/(sqrt(r(N))))
}


*add significance stars
if ``var'_p'>.1 {
	local `var'_star=""
			}
if ``var'_p' <= .1 & ``var'_p' > .05 {
		local `var'_star="*"
		}
	if ``var'_p' <= .05 & ``var'_p' > .01 {
		local `var'_star="**"
		}
	if ``var'_p' <= .01 {
		local `var'_star="***"
		}

		
if ``var'_p_con'>.1 {
	local `var'_star_con=""
			}
if ``var'_p_con' <= .1 & ``var'_p' > .05 {
		local `var'_star_con="*"
		}
	if ``var'_p_con' <= .05 & ``var'_p' > .01 {
		local `var'_star_con="**"
		}
	if ``var'_p_con' <= .01 {
		local `var'_star_con="***"
		}
}

*num observations
if `experiment'==3 {
count if exp3_treat==1
local num_treat1_mean = r(N)
count if exp3_treat==2
local num_treat2_mean = r(N)
gen num = r(N)
label var num "N"
count if exp3_treat==0
local num_cntrl_mean = r(N)
}
else {
count if treat==1
local num_treat_mean = r(N)
gen num = r(N)
label var num "N"
count if treat==0
local num_cntrl_mean = r(N)
}

label var payment_amount3 "Amount Donated"
label var var13 "Number of Gifts"
label var retention "Retention rate"
label var renewing "Percent Donating"

if `experiment'==1 | `experiment'==2 {
foreach var of varlist renewing  {
			file write fh "\hspace{4mm}"`"`: var label `var''"'" & " %4.2f (``var'_treat_mean') " & " %4.2f (``var'_cntrl_mean') " & " %4.2f (``var'_p') "``var'_star' \\" _n 
			}
foreach var of varlist  payment_amount3 var13 {
			file write fh "\hspace{4mm}"`"`: var label `var''"'" & " %4.2f (``var'_treat_mean') " & " %4.2f (``var'_cntrl_mean') " & " %4.2f (``var'_p') "``var'_star' \\" _n 
			file write fh " &  (" %4.2f (``var'_treat_se') ") & (" %4.2f (``var'_cntrl_se') ") & & \\" _n  
			}

foreach var of varlist payment_amount3 {
	label var payment_amount3 "Amount \\$\mid\\$ Donated"
			file write fh "\hspace{4mm}"`"`: var label `var''"'" & " %4.2f (``var'_treat_mean_con') " & " %4.2f (``var'_cntrl_mean_con') " & " %4.2f (``var'_p_con') "``var'_star_con' &\\" _n 
			file write fh " &  (" %4.2f (``var'_treat_se_con') ") & (" %4.2f (``var'_cntrl_se_con') ") & & \\" _n  
		}
foreach var of varlist retention {
		file write fh "\hspace{4mm}"`"`: var label `var''"'" & " %4.2f (``var'_treat_mean') " & " %4.2f (``var'_cntrl_mean') " & " %4.2f (``var'_p') "``var'_star' &\\" _n 
		}
	file write fh "\hline" _n
	file write fh "\hspace{4mm}"`"`: var label num'"'" & " %15.0fc (`num_treat_mean') " & " %15.0fc (`num_cntrl_mean') " & &  \\"
	file write fh "\hline \hline" _n
	file write fh "\vspace{0pt}" _n
}
else {

foreach var of varlist renewing  {
			file write fh "\hspace{4mm}"`"`: var label `var''"'" & " %4.2f (``var'_treat2_mean') " & " %4.2f (``var'_treat1_mean') " & " %4.2f (``var'_cntrl_mean') " & " %4.2f (``var'_p') "``var'_star' \\" _n 
			}
foreach var of varlist payment_amount3 var13 {
			file write fh "\hspace{4mm}"`"`: var label `var''"'" & " %4.2f (``var'_treat2_mean') " & " %4.2f (``var'_treat1_mean') " & " %4.2f (``var'_cntrl_mean') " & " %4.2f (``var'_p') "``var'_star' \\" _n 
			file write fh " &  (" %4.2f (``var'_treat2_se') ") & (" %4.2f (``var'_treat1_se') ") & (" %4.2f (``var'_cntrl_se') ") & \\" _n  
			}

foreach var of varlist payment_amount3 {
	label var payment_amount3 "Amount \\$\mid\\$ Donated"
	*local payment_amount3lab="\em{unconditional on donating}"
	
			file write fh "\hspace{4mm}"`"`: var label `var''"'" & " %4.2f (``var'_treat2_mean_con') " & " %4.2f (``var'_treat1_mean_con') " & " %4.2f (``var'_cntrl_mean_con') " & " %4.2f (``var'_p_con') "``var'_star_con' \\" _n 
			file write fh " &  (" %4.2f (``var'_treat2_se_con') ") & (" %4.2f (``var'_treat1_se_con') ") & (" %4.2f (``var'_cntrl_se_con') ") & \\" _n  
foreach var of varlist retention {
		file write fh "\hspace{4mm}"`"`: var label `var''"'" & " %4.2f (``var'_treat2_mean') " & " %4.2f (``var'_treat1_mean') " & " %4.2f (``var'_cntrl_mean') " & " %4.2f (``var'_p') "``var'_star' \\" _n 
		}

}
file write fh "\hline" _n
file write fh "\hspace{4mm}"`"`: var label num'"'" & " %15.0fc (`num_treat2_mean') " & " %15.0fc (`num_treat1_mean') " & " %15.0fc (`num_cntrl_mean') " &  \\"
file write fh "\hline \hline" _n
}

file write fh "\end{tabular}" _n
}
file write fh "\end{subtable}" _n

file close fh 

pause