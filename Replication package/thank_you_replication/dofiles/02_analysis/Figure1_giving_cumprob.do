********************************************************************************
*Purpose: This do file creates Figure 1
*Input Files: temp_data_exp1_exp3.dta
*Output Files: figure1_giving_cumprob.pdf
********************************************************************************


clear
cd "$dir/data"

use temp_data_exp1_exp3, clear

***cleaning dataset

keep if date>=td(01jul2011) & date<=td(31oct2017)
keep if exec_date!="1610" & exec_date!="1701" 
drop if station=="24" | station=="55" | station=="64" | station=="61"

count if treat==1
local treat_group = 445641
count if treat==0
local control_group=55434
gen gift=1
gen monthyear = mofd(date)
format monthyear %tmMonCCYY
format date %td
gen weeks_since_edate=round((date - edate)/7)


cd "$dir/latex"

keep if weeks_since_edate>0
sort account_id weeks_since_edate
		by account_id: gen dup=_n
		drop if dup>1

collapse(sum) gift if weeks_since_edate<=260, by(weeks_since_edate treat)
tsset treat weeks_since_edate
sort treat weeks_since_edate
by treat: gen cum_gift_amount=sum(gift)
replace cum_gift_amount=cum_gift_amount/`treat_group' if treat==1
replace cum_gift_amount=cum_gift_amount/`control_group' if treat==0

***creating figure 

twoway (line cum_gift_amount weeks_since_edate if treat==1 , lcolor(red) lwidth(med))/*
*/ (line cum_gift_amount weeks_since_edate if treat==0 , lpattern(shortdash) lcolor(blue) lwidth(med)),/*
*/ legend(order(1 "Treatment" 2 "Control")) ytitle("Cum. Prob. Donating") xlabel(0 (25) 260) xtitle("Weeks from Treatment")/*
*/ graphregion(color(white)) bgcolor(white) 
graph export "figure1_giving_cumprob.pdf", as(pdf) font("Times New Roman") replace
