/**********************************************************************************************
*This dofile can only be run after the running all dofiles located in the dofiles/dataprep folder
* Purpose: Runs all do-files located in the dofiles/analysis folder
* Date modified: 2021 

*Instructions: Uncomment line 32 after data from UAS has been received and all do files
in the dataprep folder have been run
**********************************************************************************************/





*===============================================================================
*  CREATING MAIN FIGURES AND TABLES
*===============================================================================

*MAIN FIGURES

*Figure 1 - Cumulative Probability of Donating in Experiment 
do "$dofiles/02_analysis/Figure1_giving_cumprob.do" 



*Figure 2 - Forecasts of Treatment Effects

/***********************************************************************************************
Uncomment below line after data from UAS has been received and all do files in the dataprep folder
have been run
***********************************************************************************************/

*do "$dofiles/02_analysis/Figure2_forecast_graph.do" 

*Figure 3 - Treatment Effect (in % Change):  Observed Versus Predicted
do "$dofiles/02_analysis/Figure3_obs_pred_don_rates.do"

*MAIN TABLES

*Table 1 - Balance Table
do "$dofiles/02_analysis/Table1_balance_table.do" 

*Table 2 - Treatment Effect
do "$dofiles/02_analysis/Table2_treatment_effects.do"


