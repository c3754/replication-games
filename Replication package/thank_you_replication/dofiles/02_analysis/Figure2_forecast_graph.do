********************************************************************************
*Purpose: This do file creates Figure 1
*Input Files: forecast_charity.dta
*Temp Files: block11.gph, block21.gph, block31.gph, block12.gph, block22.gph, block32.gph, figure2_forecast_graph.gph
*Output Files: figure2_forecast_graph.eps
********************************************************************************

cd "$dir/data"
use forecast_charity, clear

//GG CODE MODIFICATIONS for GRAPHS changes codes to use results from summarize instead of expxperc (?) NB that both vars in the data fail a Francia test for normality so appropriate to not assume normality for the CI
set seed 1984
tempfile bstraprestore
save `bstraprestore'
forvalues m=1/3{
drop if proj~=1.5 & exp~=`m'
bootstrap r(mean), reps(500): summarize (percent)  
local exp`m'_perc=e(b)[1,1]
local lo_perc`m'=e(ci_bc)[1,1]
local hi_perc`m'=e(ci_bc)[2,1]

bootstrap r(mean),reps(500): summarize (amount)
local exp`m'_am=e(b)[1,1]
local lo_am`m'=e(ci_bc)[1,1]
local hi_am`m'=e(ci_bc)[2,1]
use `bstraprestore',clear
}
//END CODE MODIFICATIONS - NOTE, THESE CONFIDENCE INTERVALS WERE NEVER ACTUALLY USED IN THE GRAPHS

/*Saving values 
foreach x of numlist 1/3 {
summarize percent if proj==1.5 & exp==`x'
local exp`x'_perc=r(mean)
summarize amount if proj==1.5 & exp==`x'
local exp`x'_am=r(mean)
local lo_perc`x'=`exp`x'_perc' - 0.05*`exp`x'_perc'
local hi_perc`x'=`exp`x'_perc' + 0.05*`exp`x'_perc'
local lo_am`x'=`exp`x'_am' - 0.05*`exp`x'_am'
local hi_am`x'=`exp`x'_am' + 0.05*`exp`x'_am'
}
*/

//-----------------------------------
* Forecast Graphs
//-----------------------------------
cd "$dir/latex"
foreach x of numlist 1/3 {
*percent graph
capture erase block`x'1.gph
twoway (bar percent proj if exp==`x' & proj==1, barwidth(0.40) fcolor(none)/*
*/ lcolor(black))(bar percent proj if exp==`x' & proj==1.5, barwidth(0.40) fcolor(emerald)/*
*/ lcolor(black))(bar percent proj if exp==`x' & proj==2, barwidth(0.40) fcolor(ltblue)/*
*/ lcolor(black)) (bar percent proj if exp==`x' & proj==2.5, barwidth(0.40) fcolor(blue)/*
*/ lcolor(black)) (rcap hi_percent lo_percent proj if exp==`x', lcolor(red)),/*
*/ xlabel(1 "Control" 1.5 "Treatment" 2 "Experts" 2.5 "Nonexperts", labsize(small))/*
*/ ytitle("Percentage Donating (%)") xtitle("")  ysca(axis(1) r(0 55)) ylab(0(20)55, nogrid) /* 
*/ graphregion(color(white)) legend(off) saving(block`x'1.gph,replace) title("Experiment `x': % Donating", size(small))

*amount graph
capture erase block`x'2.gph
twoway (bar amount proj if exp==`x' & proj==1, barwidth(0.40) fcolor(none)/*
*/ lcolor(black)) (bar amount proj if exp==`x' & proj==1.5, barwidth(0.40) fcolor(emerald)/*
*/ lcolor(black)) (bar amount proj if exp==`x' & proj==2, barwidth(0.40) fcolor(ltblue)/*
*/ lcolor(black)) (bar amount proj if exp==`x' & proj==2.5, barwidth(0.40) fcolor(blue)/*
*/ lcolor(black)) (rcap hi_amount lo_amount proj if exp==`x', lcolor(red)),/*
*/ xlabel(1 "Control" 1.5 "Treatment" 2 "Experts" 2.5 "Nonexperts", labsize(small))/*
*/ ysca(axis(1) r(0 175)) ylab(0(40)175, nogrid) ytitle("Gift amount ($)") xtitle("") /* 
*/ title("Experiment `x': Gift Amount", size(small)) graphregion(color(white)) legend(off) saving(block`x'2.gph,replace)
}


capture erase forecast_graph.gph
graph combine block11.gph block12.gph/*
*/ block21.gph block22.gph block31.gph block32.gph, rows(3) cols(2) graphregion(color(white))/*
*/ saving(figure2_forecast_graph.gph, replace) ysize(7)
graph export figure2_forecast_graph.eps, as(eps) preview(on) replace

**erasing temp files 

foreach x of numlist 1/3 {
	capture erase block`x'1.gph
	capture erase block`x'2.gph
	
}

capture erase figure2_forecast_graph.gph
