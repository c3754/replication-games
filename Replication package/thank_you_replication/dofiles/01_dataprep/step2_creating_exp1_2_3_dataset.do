********************************************************************************
*Purpose: Creating final analysis files for exp1, exp2 and exp3 which are used to create:
*Figure 2 and Figure A2. 
*Table 1, Table 2, Table A1, Table A2, Table A3, Table A4, Table A6
  
*Input Files: reshaped_temp_data_exp1_exp3.dta, gift_exp2temp.dta
*Output Files: exp1_analysis_data.dta, exp2_analysis_data, exp3_analysis_data
********************************************************************************

cd "$dir/data"
use reshaped_temp_data_exp1_exp3,clear

**DROP MISSING PAYMENT AMOUNT 2
drop if payment_amount2==0


*Experiment 1
*------------------------------
	label var inc_display4 "\%Income below \\\$34,000"
	label var inc_display3 "\%Income \\\$35,000-\\\$99,999"
	label var inc_display1 "\%Income \\\$100,000-\\\$174,999"
	label var inc_display2 "\%Income \\\$175,000+"
	label var female_missing "Gender missing"
	label var age_income_missing "Age/Income missing"
	label var female "\%Female"
	label var age_display1 "\%18 to 44 years old"
	label var age_display2 "\%45 to 64 years old"
	label var age_display3 "\%65+ years old"

*donated and conditional outcome 
		gen donated = 1 if var13>0 & !missing(var13)
		replace donated=0 if var13==0
		*generate gift conditional on donating 
		gen gift_cond = payment_amount3 if payment_amount3>0
		label var donated "Donated"
		label var gift_cond "Gift Amount Conditional"
		
*redo residence length:
replace lor_display2=1 if lor_display3==1
	label var lor_display1 "\%Residence length$<$5 years"
	label var lor_display2 "\%Residence length$>5$ years"
	drop lor_display3

	
keep if exec_date!="1610" & exec_date!="1701"


*This drop is to reconcile the treatment imbalance, we take out three stations with no control observations
drop if station=="24" | station=="55" | station=="64" | station=="61"


save exp1_analysis_data, replace
	


*Experiment 2
*-----------------------
clear
use gift_exp2temp
collapse (sum) payment_amount var1, by(timeline account_id treat response_type)
encode timeline, gen(t)
drop timeline

	reshape wide payment_amount var1, i(account_id treat) j(t) 

	drop if payment_amount1!=. //drop existing donors 

	replace payment_amount2=0 if payment_amount2==.
	replace payment_amount3=0 if payment_amount3==.
	replace payment_amount4=0 if payment_amount4==.

	replace var12=0 if var12==.
	replace var13=0 if var13==.
	replace var14=0 if var14==.
	
	gen retention=payment_amount3/payment_amount2
	gen retention2=payment_amount4/payment_amount2
	
	gen renewing=0
	replace renewing=1 if var13!=0

*donated and conditional outcome 
		gen donated = 1 if var13>0 & !missing(var13)
		replace donated=0 if var13==0
		*generate gift conditional on donating 
		gen gift_cond = payment_amount3 if payment_amount3>0
		label var donated "Donated"
		label var gift_cond "Gift Amount Conditional"
	
rename treat treatment
encode treatment, gen(treat)
replace treat = 0 if treat==2

label var payment_amount2 "Baseline Gift Amount"
label var var12 "Baseline Number of Gifts" 
label var renewing "\% Returning Donor"
save exp2_analysis_data, replace 


*Experiment 3
clear
use reshaped_temp_data_exp1_exp3

**DROP MISSING PAYMENT AMOUNT 2
drop if payment_amount2==0


*redo residence length:
replace lor_display2=1 if lor_display3==1
	label var lor_display1 "\%Residence length$<$5 years"
	label var lor_display2 "\%Residence length$>5$ years"
	drop lor_display3
	
*labels for the latex file
	label var inc_display4 "\%Income below \\\$34,000"
	label var inc_display3 "\%Income \\\$35,000-\\\$99,999"
	label var inc_display1 "\%Income \\\$100,000-\\\$174,999"
	label var inc_display2 "\%Income \\\$175,000+"
	label var female_missing "Gender missing"
	label var age_income_missing "Age/Income missing"
	label var female "\%Female"
	label var age_display1 "\%18 to 44 years old"
	label var age_display2 "\%45 to 64 years old"
	label var age_display3 "\%65+ years old"
	

*donated and conditional outcome 
		gen donated = 1 if var13>0 & !missing(var13)
		replace donated=0 if var13==0
		*generate gift conditional on donating 
		gen gift_cond = payment_amount3 if payment_amount3>0
		label var donated "Donated"
		label var gift_cond "Gift Amount Conditional"

*drop rules for Experiment 3, we only keep 1601
gen exp3 = 1 if edate==td(01oct2016)
replace exp3=0 if edate!=td(01oct2016)


keep if exp3==1
gen exp3_treat = 0 if script2==0 & treat==0
replace exp3_treat=1 if script2==0 & treat==1
replace exp3_treat=2 if script2==1 & treat==1


*This drop is to reconcile the treatment imbalance, we take out three stations
*with no control observations
drop if station=="24" | station=="55" | station=="64" | station=="61"

label define exp3_label 0 "Control" 1 "Old Script" 2 "New Script"
label values exp3_treat exp3_label

save exp3_analysis_data, replace
