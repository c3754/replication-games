/********************************************************************************
* Purpose: This do file creates the dataset to be used for creation of Figure 2 

*Input Files: charity.dta, exp1_analysis_data.dta, exp2_analysis_data, exp3_analysis_data, 
uas_dataset.dta
*Temp Files: charity_temp.dta
*Output Files: forecast_charity.dta
********************************************************************************/


cd "$dir/data"

***creating charity_temp

use "charity",clear

*proportion correct 
foreach x of numlist 1/3 {
summarize exp`x'_percentactual
gen exp`x'_percentcorr=1 if exp`x'_percent<=(r(mean) + .05*r(mean)) & exp`x'_percent>=(r(mean) - .05*r(mean))
summarize exp`x'_amountactual
gen exp`x'_amountcorr=1 if exp`x'_amount<=(r(mean) + .05*r(mean)) & exp`x'_amount>=(r(mean) - .05*r(mean))
replace exp`x'_percentcorr=0 if !missing(exp`x'_percent) & missing(exp`x'_percentcorr)
replace exp`x'_amountcorr=0 if !missing(exp`x'_amount) & missing(exp`x'_amountcorr)
}
//--------------------------------------------------------
*Charity predicted vs actual gaps
//--------------------------------------------------------

foreach x of numlist 1/3 {
gen exp`x'_gapamount = exp`x'_amount - exp`x'_amountactual
gen exp`x'_gappercent= exp`x'_percent - exp`x'_percentactual
}
egen gap_amount= rowmean(exp*_gapamount)
egen gap_percent = rowmean(exp*_gappercent)

label var gap_amount "Overprediction (amount)"
label var gap_percent "Overprediction (percent)"


   
collapse (mean) exp1_* exp2_* exp3_* confidence (semean) exp1_percent_se=exp1_percent/*
*/ exp1_amount_se=exp1_amount exp2_percent_se=exp2_percent exp3_percent_se=exp3_percent/*
*/ exp2_amount_se=exp2_amount exp3_amount_se=exp3_amount

foreach x of numlist 1/3 {
	foreach var in amount percent percent_se amount_se {
	rename exp`x'_`var' `var'`x'
	}
}


gen x=1
reshape long amount percent percent_se amount_se, i(x) j(exp)

gen hi_amount=amount+(amount_se*2)
gen hi_percent=percent+(percent_se*2)
gen lo_amount=amount-(amount_se*2)
gen lo_percent=percent-(percent_se*2)
gen proj=2


save "charity_temp.dta", replace

***adding in actual values

*--------------------------------------------------------
*Experimental Sample
*--------------------------------------------------------
foreach x of numlist 1/3 {
use exp`x'_analysis_data, clear
gen amount=payment_amount3 if renewing==1

*grab obsv for table
count if !missing(amount)
local num_`x'_exp=r(N)

collapse (mean) amount renewing (semean) percent_se=renewing/*
*/ amount_se=payment_amount2, by(treat)
rename renewing percent
replace percent=100*percent
replace percent_se=100*percent_se
gen hi_amount=amount+(amount_se*2)
gen hi_percent=percent+(percent_se*2)
gen lo_amount=amount-(amount_se*2)
gen lo_percent=percent-(percent_se*2)
gen proj=1 if treat==0
replace proj=1.5 if treat==1
gen exp=`x'
append using charity_temp
save charity_temp, replace
}



*--------------------------------------------------------
*Adding to UAS Sample*
*--------------------------------------------------------
cd "$dir/data"
use uas_dataset, clear

*proportion correct guessing by experiment
foreach x of numlist 1/3 {
summarize exp`x'_percentactual
gen exp`x'_percentcorr=1 if exp`x'_percent<=(r(mean) + .05*r(mean)) & exp`x'_percent>=(r(mean) - .05*r(mean))
summarize exp`x'_amountactual
gen exp`x'_amountcorr=1 if exp`x'_amount<=(r(mean) + .05*r(mean)) & exp`x'_amount>=(r(mean) - .05*r(mean))
replace exp`x'_percentcorr=0 if !missing(exp`x'_percent) & missing(exp`x'_percentcorr)
replace exp`x'_amountcorr=0 if !missing(exp`x'_amount) & missing(exp`x'_amountcorr)
}

foreach x of numlist 1/3  {
label var exp`x'_percent "Experiment `x' (percent)"
label var exp`x'_amount "Experiment `x' (amount)"
replace num_correct=num_correct+1 if exp`x'_percent> (exp`x'_percentactual-(0.05*exp`x'_percentactual))/*
		*/ & exp`x'_percent<(exp`x'_percentactual+(0.05*exp`x'_percentactual))
	
	}
	
label var num_correct "Number Correct"

drop exp*actual
   

collapse (mean) exp1_* exp2_* exp3_* (semean) exp1_percent_se=exp1_percent/*
*/  exp2_percent_se=exp2_percent exp3_percent_se=exp3_percent exp1_amount_se=exp1_amount/*
*/  exp2_amount_se=exp2_amount exp3_amount_se=exp3_amount 

foreach x of numlist 1/3 {
	foreach var in percent percent_se amount amount_se{
	rename exp`x'_`var' `var'`x'
	}
}

gen x=1
reshape long percent percent_se amount amount_se, i(x) j(exp)
replace exp=1 if exp==4
replace exp=2 if exp==5
replace exp=3 if exp==6

gen hi_percent=percent+(percent_se*2)
gen lo_percent=percent-(percent_se*2)
gen hi_amount=amount+(amount_se*2)
gen lo_amount=amount-(amount_se*2)

gen proj=2.5

append using charity_temp

save "forecast_charity",replace

erase "$dir/data/charity_temp.dta"
