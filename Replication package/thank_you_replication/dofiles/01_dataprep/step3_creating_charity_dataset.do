/********************************************************************************
* Purpose: This do file creates the dataset used for creation of
Figure A3, Figure A4, Figure A5, Table A5 

*Input Files: charity.xlsx
*Output Files: charity.dta
********************************************************************************/


cd "$dir/data"
import excel "charity.xlsx", firstrow clear

//--------------------------------------------------------
*clean outcome variables
//--------------------------------------------------------
*public tv exp 1
rename Q12 exp1_percent
rename Q14 exp1_amount
*public tv with enhanced script exp3
rename Q17 exp3_percent
rename Q15 exp3_amount

*non profit
rename Q18 exp2_percent
rename Q30 exp2_amount
*confidence
rename Q20 confidence

*other variables
rename Q7 org_program
replace org_program="Other" if org_program=="Other:"
encode org_program, gen(org_type)

foreach var of varlist exp1_percent exp1_amount exp2_percent exp2_amount/*
*/ exp3_percent exp3_amount confidence {
	replace `var'="." if `var'=="-99"
	destring `var', replace
}
gen exp1_percentactual=28
gen exp1_amountactual=126.24
gen exp2_percentactual=31
gen exp2_amountactual=116.01
gen exp3_percentactual=27
gen exp3_amountactual=114.27

gen num_correct=0
foreach x of numlist 1/3  {
	foreach var in amount percent {
		label var exp`x'_`var' "Experiment `x' (`var')"
		replace num_correct=num_correct+1 if exp`x'_`var'> (exp`x'_`var'actual-(0.05*exp`x'_`var'actual))/*
		*/ & exp`x'_`var'<(exp`x'_`var'actual+(0.05*exp`x'_`var'actual))
	}
}

count if !missing(exp1_percent) & !missing(exp1_amount)
local num_1_char=r(N)
count if !missing(exp2_percent) & !missing(exp2_amount)
local num_2_char=r(N)
count if !missing(exp3_percent) & !missing(exp3_amount)
local num_3_char=r(N)


//--------------------------------------------------------
*cleaning some more variables
//--------------------------------------------------------

label var confidence "Confidence"
label var num_correct "Number Correct"

gen exp_org=1 if Q10=="Less than 1 year"
gen exp_industry=1 if Q11=="Less than 1 year"
replace exp_org=2 if Q10=="1-5 years"
replace exp_industry=2 if Q11=="1-5 years"
replace exp_org=3 if Q10=="6 or more years"
replace exp_industry=3 if Q11=="6 or more years"
label define experience 1 ">1 year" 2 "1-5 years" 3 "6+ years"
label values exp_org experience
label values exp_industry experience

gen num_donations=1 if Q6=="100 to 999" | Q6=="Fewer than 100"
replace num_donations=2 if Q6=="1,000 to 9,999"
replace num_donations=3 if Q6=="10,000 to 99,999"
replace num_donations=4 if Q6=="100,000+"

gen does_calls = 1 if Q21=="Always" | Q21=="Often" | Q21=="Sometimes"
replace does_calls=0 if Q21=="Rarely" | Q21=="Never"



label var exp_org "Experience (organization)"
label var exp_industry "Experience (industry)"
label var does_calls "Does Calls"
label var num_donations "No. of Donations"

gen highexp_ind = 1 if exp_industry==3
replace highexp_ind=0 if exp_industry==1 | exp_industry==2

gen highexp_org=1 if exp_org==3
replace highexp_org=0 if exp_org==1 | exp_org==2

gen high_don=1 if num_donations==3 | num_donations==4
replace high_don=0 if num_donations==1 | num_donations==2

label var high_don "10k+ donations/year"
label var highexp_org "Experience 5+ years (org.)"
label var highexp_ind "Experience 5+ years (industry)"

save "charity.dta",replace
