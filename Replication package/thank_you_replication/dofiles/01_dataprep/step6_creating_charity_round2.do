/********************************************************************************
* Purpose: This do file creates the charity dataset to be used for analysis of the
second round of surveys.

This dataset is used in the creation of Figure A6

*Input Files: spi2019_practitioner.xlsx, experiment_data.xlsx, charity_more_questions.csv
*Temp Files: spi_2019.dta actual_experiment_data.dta spi_mailinglist.dta charity_morequestions.dta 
*Output Files: charity_round2.dta
********************************************************************************/

cd "$dir/data"

**cleaning SPI Practitioner data

import excel "spi2019_practitioner.xlsx", sheet("Sheet1") firstrow clear
gen version="spi2019"
rename door_1 volunteer_1 
rename door_3 volunteer_2 
rename door_2 volunteer_3
rename door_4 volunteer_4
rename door_confidence volunteer_confidence
foreach x in 1 2 3 confidence {
rename announcements_`x' door_`x'
rename callingin_`x' calling_`x'
}

gen do_door=1 if inlist(solicitation_activities,"1")
gen do_mail =1 if inlist(solicitation_activities,"2")
gen do_call=1 if inlist(solicitation_activities,"3")
gen do_match=1 if inlist(solicitation_activities,"4")
gen do_gift=1 if inlist(solicitation_activities,"5")
gen do_premium=1 if inlist(solicitation_activities,"6")
gen do_paid=1 if inlist(solicitation_activities,"7")
gen do_volunteer =1 if inlist(solicitation_activities,"8")

save "spi_2019.dta", replace 

**saving actual experiment data

import excel "experiment_data.xlsx", firstrow clear
gen tempid=1
save "actual_experiment_data", replace 

**saving SPI data from mailings

import delimited "spi_mailing_list.csv", varnames(1) clear
gen version="spi_mail"
save "spi_mailinglist.dta", replace 

**saving Fundraising Professionals Survey

import delimited "charity_more_questions.csv", varnames(1) clear
save charity_morequestions, replace

//-------------------------
*forecast 
//-------------------------
use charity_morequestions,clear
local today td(12march2020)
local future td(20march2020)


//-------------------------------------
*make charts
//--------------------------------------
gen version="main_run"
append using "spi_mailinglist.dta"

*renaming variables
rename q17_3 mail_1
rename q17_4 mail_2
rename q15_8 mail_3
rename q15_6 mail_4
rename q59_1 mail_confidence
rename q50 mail_read

rename q54_2 volunteer_1
rename q55_1 volunteer_2
rename v54   volunteer_3
rename q56_1 volunteer_4
rename q62_1 volunteer_confidence
rename q49 volunteer_read

rename q18_3 calling_1
rename q18_4 calling_2
rename q18_5 calling_3
rename q60_1 calling_confidence
rename q48 calling_read

rename q51_2 matching_1
rename q51_3 matching_2
rename q51_4 matching_3
rename q52_5 matching_4
rename q52_6 matching_5
rename q52_7 matching_6
rename q61_1 matching_confidence
rename q44 matching_read

rename q12_9 door_1
rename q14_3 door_2
rename q53_4 door_3
rename q58_1 door_confidence
rename q46 door_read

rename v76 agency_1
rename v77 agency_2
rename v78 agency_3
rename q63_1 agency_4
rename v80 agency_confidence
rename q47 agency_read


foreach var in mail volunteer calling matching door agency {
	foreach x of numlist 1/6 {
		capture destring `var'_`x', replace
	}
	destring `var'_confidence, replace
}

append using "spi_2019.dta"


replace do_door=1 if !missing(q67_7)
replace do_mail =1 if !missing(q67_8)
replace do_match=1 if !missing(q67_1)  
replace do_gift=1 if !missing(q67_5)
replace do_premium=1 if !missing(q67_6)
replace do_paid=1 if !missing(q67_4)
replace do_volunteer =1  if !missing(q67_3)
gen do_info=1 if !missing(q67_9)
gen do_agency=1 if !missing(q67_12)

*load in actual values
gen tempid=1
merge m:1 tempid using "actual_experiment_data"
drop _merge tempid
save "charity_round2",replace

*erasing temp files

erase "spi_2019.dta"
erase "spi_mailinglist.dta"
erase "charity_morequestions.dta" 
erase "actual_experiment_data.dta" 
