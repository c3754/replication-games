/**********************************************************************************************
* Purpose: Runs all do-files located in the dofiles/dataprep folder
* Date modified: 2021 

*Instructions: Uncomment line 22 and 29 after data from UAS has been received and all do files
in the dataprep folder have been run
**********************************************************************************************/

*===============================================================================
*  CREATING DATASETS
*===============================================================================

do "$dofiles/01_dataprep/step1_creating_temp_datasets.do" 
do "$dofiles/01_dataprep/step2_creating_exp1_2_3_dataset.do" 
do "$dofiles/01_dataprep/step3_creating_charity_dataset.do" 

/***********************************************************************************************
Uncomment below line after data from UAS has been received and all do files in the dataprep folder
have been run
***********************************************************************************************/

*do "$dofiles/01_dataprep/step4_creating_uas_dataset.do" 

/***********************************************************************************************
Uncomment below line after data from UAS has been received and all do files in the dataprep folder
have been run
***********************************************************************************************/

*do "$dofiles/01_dataprep/step5_creating_forecast_sample_figure2.do" 

do "$dofiles/01_dataprep/step6_creating_charity_round2.do" 
