/********************************************************************************
* Purpose: This do file creates, cleans, and merges files to create the datasets 
  out of which final data for experiment 1, 2 and 3 will be extracted for analysis. 
  It also creates the final dataset used for the creation of Figure 1

*Input Files: transactions.dta, callog.dta, demographics.dta, treatments.dta, gift.dta
*Temp Files: transaction_temp.dta, callog_temp.dta, demographics_temp.dta, treatments_temp.dta
*Output Files: temp_data_exp1_exp3, reshaped_temp_data_exp1_exp3, gift_exp2temp
********************************************************************************/

cd "$dir/data"

/////////////////////////////////////////////////////////
////												/////
//// 	     PREPARE TRANSACTION DATA				/////
////												/////
/////////////////////////////////////////////////////////

use "transactions", clear //transaction log

split gift_date, p(" ") //all of the dates have an hour attached
drop gift_date2
drop gift_date
ren gift_date1 gift_date

gen date=date(gift_date, "YMD")
format date %d //format date
sort account_id date

//Create variables for gift type - installment, or one-time

	gen gift_kind1=0 if gift_kind=="IN" 
	replace gift_kind1=1 if gift_kind=="OP" 
	replace gift_kind1=2 if source_category=="SG" 
	replace gift_kind1=3 if gift_kind=="NULL"
	replace gift_kind1=4 if gift_kind1==. //all other types
	
	gen pledge=0
	replace pledge=1 if source_category=="P" //identify pledge gifts 

	label define source_definition 0 "Installment" 1 "One time payment" 2 "Sustaining Gift" 3 "Other" 4 "Unknown"
	label values gift_kind1 source_definition

//Create variables for solicitation type
 
	gen technique1=0 if technique=="FF"
	replace technique1=1 if technique=="IN"
	replace technique1=2 if technique=="MM"
	replace technique1=3 if technique=="OT"
	replace technique1=4 if technique1==. //all other types

	label define technique_definition 0 "Face to Face" 1 "Internet" 2 "Mailing" 3 "Other" 4 "Unknown"
	label values technique1 technique_definition

//Create numeric variables for rejoin type - new, renewing

	gen gift_type1=0 if gift_type=="NW"
	replace gift_type1=1 if gift_type=="RN"
	replace gift_type1=2 if gift_type=="RJ"
	replace gift_type1=3 if gift_type=="AD"
	replace gift_type1=4 if gift_type=="UP"
	replace gift_type1=5 if gift_type=="OT"
	replace gift_type1=6 if gift_type1==. //all other types
	
	label define gift_definition 0 "New" 1 "Renewal" 2 "Rejoin" 3 "Additional" 4 "Upgrade" 5 "Other" 6 "Unknown"
	label values gift_type1 gift_definition
	
	
	
//Rename variables

	drop technique gift_type gift_kind
	ren technique1 technique
	ren gift_type1 gift_type
	ren gift_kind1 gift_kind
	label var technique "Solicitation type"
	label var gift_kind "Gift Kind-Onetime or Inst"
	label var gift_type "Gift Type-New or Renew"
	label var date "Date of gift (transactions)"
	label var account_id "Account ID"
	label var gift_date "Formatted Date of gift (transactions)"
	label var payment_amount "Donation amount"	

	
	replace account=upper(account) //standardize account ID
	replace station=upper(station)
	
//Collapse all so you have only one payment per day
//Recode to min giftkind
	collapse (sum) payment_amount (min) gift_kind technique gift_type pledge, by(account_id date station gift_date) 
	
	
//Format transaction dates

	gen transaction_month=month(date)
	gen transaction_year=year(date)


	//A few IDs give above $10,000 or more at some point. Tag all of them.
	
	gen big_donor_temp=1 if payment_amount>=10000
	bysort account: egen big_donor=sum(big_donor_temp)

	
save transaction_temp, replace

/////////////////////////////////////////////////////////
////												/////
//// 			PREPARE CALL DATA					/////
////												/////
/////////////////////////////////////////////////////////

use "callog", clear //log of calls


	replace account=upper(account) //standardize account ID
	replace station=upper(station)
	
//Drop anyone who was accidentally in the experiment two times

	duplicates tag account, gen(t) //this drops 0.26% of the calls (1,139)
	drop if t>0
	
// Label variables
*Changed from default of DM20Y provided by authors for DATE_CALL AND DATE_GIFT. DATE was originally coded as MDY.
	gen date_call=date(int_contact_date, "MDYhm")
	format date_call %d
	label var date_call "Date of call from call log"
	
	gen date_gift=date(gift_date, "MDY")
	format date_gift %d
	label var date_gift "Date of gift from call log"

	ren segment treatment
	label var treatment "Treatment"
	gen treat=0 if treatment=="Control"
	replace treat=1 if treatment=="Active Calls"
	label var treat "Treatment 0/1"
	
	gen date=date(gift_date, "MDY") 
	format date %d 
	label var date "Date of gift (call log)"
	label var account "Account ID"
	sort account_id date
	
	label var exec_date "YYMM of TY Group"
	
//Generate response type

	gen response_type1=0 if response_type=="LEFTMSG"
	replace response_type1=1 if response_type=="NEGENG"
	replace response_type1=2 if response_type=="NOCALL"
	replace response_type1=3 if response_type=="NOREACH"
	replace response_type1=4 if response_type=="POSENG"
	replace response_type1=5 if response_type=="REACHED"
	replace response_type1=6 if response_type1==.
	
	label define response_label 0 "Left Message" 1 "Negative Eng" 2 "Control" 3 "Not reached" 4 "Positive Eng" 5 "Neutral Eng" 6 "Unknown"
	label values response_type1 response_label
	label var response_type1 "Response Type"

//Generate engagement dummies

	gen reached=0 if treat==1
	replace reached=1 if response_type=="POSENG" | response_type=="NEGENG" | response_type=="REACHED" | response_type=="LEFTMSG"
	label var reached "Reached, 0/1 if treated"

	gen positive_engagement=0 if treat==1
	replace positive_engagement=1 if response_type=="POSENG"
	label var positive_engagement "Positive, 0/1 if treated"

	gen neutral_engagement=0 if treat==1
	replace neutral_engagement=1 if response_type=="REACHED"
	label var neutral_engagement "Positive, 0/1 if treated"

	gen negative_engagement=0 if treat==1
	replace negative_engagement=1 if response_type=="NEGENG"
	label var negative_engagement "Negative, 0/1 if treated"

	gen left_message=0 if treat==1
	replace left_message=1 if response_type=="LEFTMSG"
	label var left_message "Left Msg, 0/1 if treated"
	

sort account date

	
	
	//Available data is through 2018Q1. That means drop exec dates on or after 17/04	
	
	drop if exec_date==1704 | exec_date==1707 | exec_date==1710 | exec_date==1801
	
//Prepare exec date data
	
	tostring exec_date, force replace
	gen exec_year = substr(exec_date, 1, 2)
	gen exec_month = substr(exec_date, 3, 2)
	gen exec_day = 1
	
	destring exec_year, force replace
	destring exec_month, force replace
	
	replace exec_year=exec_year+2000 
	
	gen edate = mdy(exec_month, exec_day, exec_year)
	
	format edate %td
	
	gen e_year=year(edate)
	gen e_month=month(edate)
	
	//****** THESE VARIABLES ESTABLISH WHEN CALLS ARE MADE RELATIVE TO THE LAST GIFT FOR TREATMENT PEOPLE ONLY
	
	gen months_to_exec_from_gift_date=.
	replace months_to_exec_from_gift_date=(edate-date_gift)/30.25 if treat==1 
	
	gen months_to_call_from_edate=.
	replace months_to_call_from_edate=(date_call-edate)/30.25 if treat==1 
	
	gen months_to_call_from_gift=.
	replace months_to_call_from_gift=(date_call-date_gift)/30.25 if treat==1 
	
drop technique // also available in other dataset

ren date date_initial_gift //only for treatment people

save callog_temp, replace

/////////////////////////////////////////////////////////
////												/////
//// 			PREPARE DEMOGRAPHIPCS		     	/////
////												/////
/////////////////////////////////////////////////////////

use "demographics", clear

*ren accountid account_id
sort account

replace account=upper(account) //standardize account ID

drop if account=="NULL" //drop those without an account
duplicates drop // we have 0.3% of data that is duplicate account IDs and information

gen demo_dont_contact=0
replace demo_dont_contact=1 if donotphone=="X" | donotthank=="X"

gen female=.
replace female=0 if gender=="M"
replace female=1 if gender=="F"



//Split into 3 groups
gen age_at_gift=""
replace age_at_gift="18 to 44 years old" if agecode==1 | agecode==2 | agecode==3
replace age_at_gift="45 to 64 years old" if agecode==4 | agecode==5
replace age_at_gift="65+ years old" if agecode==6 | agecode==7

//Split into 4 groups
gen income=""
replace income="Below $34,000" if hhincomecode=="A" | hhincomecode=="B" | hhincomecode=="C"
replace income="$35,000-$$99,999" if hhincomecode=="D" | hhincomecode=="E" | hhincomecode=="F"
replace income="$100,000-$174,999" if hhincomecode=="G" | hhincomecode=="H" | hhincomecode=="I"
replace income="$175,000+" if hhincomecode=="J" | hhincomecode=="K" | hhincomecode=="L"
replace income="" if hhincomecode=="U" 

//Split into 3 groups
gen lorgroup=""
replace lorgroup="5 years or less" if lor==2 | lor==3 | lor==4
replace lorgroup="6-15 years" if lor==5 | lor==6
replace lorgroup="16+ years" if lor==7 | lor==8

//Add indicators for missing data
gen age_income_missing=0
replace age_income_missing=1 if agecode==.

gen female_missing=0
replace female_missing=1 if female==.

global var demographics female age_at_gift income lorgroup age_income_missing

save demographics_temp, replace

/////////////////////////////////////////////////////////
////												/////
//// 			PREPARE TREATMENTS       			/////
////												/////
/////////////////////////////////////////////////////////

//Experiment 3 treatments are exec-date 1610.

use "treatments", clear

replace account=upper(account) //standardize account ID

duplicates drop  //1 ID is a duplicate

save treatments_temp, replace


/////////////////////////////////////////////////////////
////												/////
//// 			MERGE DATASETS						/////
////												/////
/////////////////////////////////////////////////////////



	use "transaction_temp", clear
	
//-----------------------------------------
// Merge demographics
//-----------------------------------------
	
	merge m:1 account_id using "demographics_temp.dta"
	
	drop _m
	
//-----------------------------------------
// Merge the call log and transaction log
//-----------------------------------------
	
	merge m:1 account_id using "callog_temp.dta"

	
	gen in_experiment=0
	replace in_experiment=1 if _m==3
	
	keep if _m==3 //these are account numbers that could be matched
	
	drop _m
	
	replace date_initial_gift=. if date_initial_gift==td(1jan1900) | treat==0 

//-----------------------------------------
// Merge in the treatments
//-----------------------------------------
	
	merge m:1 account_id using "treatments_temp.dta"
	drop if _merge==2

//-----------------------------------------------
// Drop big donors
//-----------------------------------------------

	drop if big_donor>=1
	
//-----------------------------------------------	
//Generate 365.25 days past exec_date
//This tags all transactions according to where they fall in the timeline
//-----------------------------------------------
	
	gen timeline=""
	replace timeline="a_existing_donor" if date<edate-365.25 
	replace timeline="b_year before" if date<=edate & date>edate-365.25 
	replace timeline="c_year after" if date>=edate & date<edate+(365.25) 
	replace timeline="d_two years after" if date>=edate+365.25 & date<edate+365.25+365.25 
	replace timeline="e_three years after" if date>=edate+365.25+365.25 & date<edate+365.25+365.25+365.25 
	replace timeline="f_four years after" if date>=edate+365.25+365.25+365.25 & date<edate+365.25+365.25+365.25+365.25
	replace timeline="g_five years after" if date>=edate+365.25+365.25+365.25+365.25 & date<edate+365.25+365.25+365.25+365.25+365.25
	replace timeline="h_six+ years after" if date>=edate+365.25+365.25+365.25+365.25+365.25
	
	
	
	gen sustaining_donors=0
	replace sustaining_donors=1 if gift_kind==2 & timeline=="b_year before"
	bysort account: egen sustaining_donors_temp=max(sustaining_donors)
	drop if sustaining_donors_temp>=1
	
	
//-----------------------------------------------	
// Get the initial gifts
//-----------------------------------------------
	
	gen difference=edate-date   //difference between exec date and date
								//positive if gift date earlier
								//negative if gift date later
	
	gen difference_months=difference/30.25
	gen difference_months_round=round(difference_months,1)
	
	gen var1=1
	
	replace difference=. if difference<0 //don't count future gifts
	
	bysort account: egen minimum_diff=min(difference) //get the gift closest to exec date
	gen diff_months=minimum_diff/30.25
	
	//*** This variable gives the months that pass from the last gift to exec date.
	
	
	gen months_to_exec_date=round(diff_months, 1)
	
	//Get the first and last dates in the file
	
    by account (date), sort: gen firstdate = date[1]
	by account (date), sort: gen lastdate = date[_N]
	
	format firstdate %tdd_m_y
	format lastdate %tdd_m_y
	gen firstdate_year=year(firstdate)
	gen firstdate_month=month(firstdate)
	gen lastdate_year=year(lastdate)
	gen lastdate_month=month(lastdate)
			
save "temp_data_exp1_exp3",replace
//GG Edited to put pledge as "min" instead of "mean"
	collapse (sum) payment_amount var1 (min) pledge, by(timeline account_id station exec_date edate treat script response_type female age_at_gift lorgroup income female_missing age_income_missing months_to_exec_date months_to_call_from_edate months_to_exec_from_gift_date months_to_call_from_gift)
	
	encode timeline, gen(t)
	replace t=0 if t==.
	
	drop timeline
	
	//Reshape wide
	reshape wide payment_amount var1 pledge, i(account_id) j(t)
	
	//drop existing donors 
	drop if payment_amount1!=. 
	
	replace payment_amount2=0 if payment_amount2==. 
	replace payment_amount3=0 if payment_amount3==. 
	replace payment_amount4=0 if payment_amount4==. & edate<date("01/01/2016","MDY") 
	replace payment_amount5=0 if payment_amount5==. & edate<date("01/01/2015","MDY") 
	replace payment_amount6=0 if payment_amount6==. & edate<date("01/01/2014","MDY") 


	replace var12=0 if var12==.
	replace var13=0 if var13==.
	replace var14=0 if var14==. & edate<date("01/01/2016","MDY")
	replace var15=0 if var15==. & edate<date("01/01/2015","MDY") 
	replace var16=0 if var16==.	& edate<date("01/01/2014","MDY") 

	
	gen retention=payment_amount3/payment_amount2
	gen retention2=(payment_amount4)/payment_amount2
	gen retention3=(payment_amount5)/payment_amount2
	
	gen renewing=0
	replace renewing=1 if var13!=0 & var13!=.

	gen age_x_treat=age_at_gift*treat
	gen female_x_treat=female*treat
	gen payment_pre_x_treat=payment_amount2*treat
	gen var12_x_treat=var12*treat
	gen income_x_treat=income*treat
	
	encode station, gen(station_id)
	encode exec_date, gen(edate_dummy)
	
	bysort station_id: egen retention_mean=mean(retention)
		
	//Generate engagement dummies

	gen reached=0 if treat==1
	replace reached=1 if response_type=="POSENG" | response_type=="NEGENG" | response_type=="REACHED" | response_type=="LEFTMSG"
	label var reached "Reached, 0/1 if treated"

	gen positive_engagement=0 if treat==1
	replace positive_engagement=1 if response_type=="POSENG"
	label var positive_engagement "Positive, 0/1 if treated"

	gen neutral_engagement=0 if treat==1
	replace neutral_engagement=1 if response_type=="REACHED"
	label var neutral_engagement "Positive, 0/1 if treated"

	gen negative_engagement=0 if treat==1
	replace negative_engagement=1 if response_type=="NEGENG"
	label var negative_engagement "Negative, 0/1 if treated"

	gen left_message=0 if treat==1
	replace left_message=1 if response_type=="LEFTMSG"
	label var left_message "Left Msg, 0/1 if treated"
	
	
		
	replace script=0 if script==. & treat!=. //script dummy stands for whether new script
	gen treatment="control" if treat==0
	replace treatment="call" if treat==1
	replace treatment="new" if script==1
	
	
	//The below creates script2 which is equal to 1 for new script and 0 otherwise
	gen script2=script
	replace script2=0 if treat==0
	label var script2 "New Call Script"

	
	tab income, gen(inc_display)
	tab age_at_gift, gen(age_display)
	tab lorgroup, gen(lor_display)
	
	label var age_display1 "18 to 44 years old"
	label var age_display2 "45 to 64 years old"
	label var age_display3 "65+ years old"
	label var inc_display4 "Income below $34,000"
	label var inc_display3 "Income $35,000-$99,999"
	label var inc_display1 "Income $100,000-$174,999"
	label var inc_display2 "Income $175,000+"
	label var lor_display1 "Residence length 5 years or less"
	label var lor_display2 "Residence length 6-15 years"
	label var lor_display3 "Residence length 16+ years"
	
	egen ii=group(station_id edate_dummy)
	xtset ii //station_id
	
	label var payment_amount3 "Gift Amount"
	label var payment_amount2 "Baseline Gift Amount"
	label var var12 "Baseline Number of Gifts"
	label var female "Female"
	label var script "New Script Treatment"
	label var treat "Call Treatment"
	
	order account_id station
	********
	save reshaped_temp_data_exp1_exp3, replace
	********
	
	*final: 600,390, 71 - variables - verified
	

	
use "gift.dta",clear
	
	
ren treatment response_type
ren paymentamounttransactions payment_amount
gen var1=1 //counter for number of transactions
ren id account_id

gen treat="Control" if response_type=="Control"
replace treat="Call" if response_type=="Called: Contacted" | response_type=="Called: Not Contacted"

gen date=date(giftdatetransaction,"MD20Y")
gen edate=date("4/1/2013","MDY")

gen year=year(date)
gen month=month(date)

gen diff=edate-date //exec date minus date


//Create timeline just like in the previous data

	gen timeline=""
	replace timeline="a_existing_donor" if date<edate-365.25 
	replace timeline="b_year before" if date<=edate & date>edate-(365.25) 
	replace timeline="c_year after" if date>=edate & date<edate+(365.25) 
	replace timeline="two years after or more" if date>=edate+365.25+365.25 
	replace timeline="na" if timeline==""
	
	save gift_exp2temp, replace

	*erasing temp files
	erase "transaction_temp.dta"
	erase "callog_temp.dta" 
	erase "demographics_temp.dta"
	erase "treatments_temp.dta"
