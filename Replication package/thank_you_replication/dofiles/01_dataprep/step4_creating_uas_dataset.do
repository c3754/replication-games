/********************************************************************************
* Purpose: This do file creates the UAS dataset which is used for creation of 
Figure A3 and Figure A4

*Input Files: uas166.dta, uas24.dta
*Output Files: uas_dataset.dta
********************************************************************************/




cd "$dir/data"
*use uas166, clear
*merge 1:1 uasid using uas24, keepusing(q453_ q454_)

//begin GG - replication games RECODE: read.csv file instead of attempting to use the proprietary format - create the UAS24.dta file for the 1:1 merge command, q453_ is text data in file, code is expecting a numeric
tempfile c1
import delimited "F:\replication-games\Replication package\thank_you_replication\data\uas24.csv", clear
save `c1',replace

import delimited "F:\replication-games\Replication package\thank_you_replication\data\uas166.csv", clear

merge 1:1 uasid using `c1', keepusing(q453_ q454_)
gen q453=1 if q453_=="1 Yes"
replace q453=5 if q453_=="5 No"
replace q453=.a if q453_==".a"
replace q453=.e if q453_==".e"

//end GG - replication games RECODE
drop _merge
gen donated_charity=1 if q454>=500 & !missing(q454)
replace donated_charity=0 if q453==5 | q454<500 & !missing(q454)
gen exp1_percentactual=28
gen exp1_amountactual=126.24
gen exp2_percentactual=31
gen exp2_amountactual=116.01
gen exp3_percentactual=27
gen exp3_amountactual=114.27
rename charity_q1cont exp1_percent
rename charity_q2cont exp3_percent
rename charity_q3cont exp2_percent
rename charity_q1_nocall exp1_amount
rename charity_q2_yescall exp3_amount
rename charity_q3_nocall exp2_amount

gen expall_percent=exp1_percent if !missing(exp1_percent)
replace expall_percent=exp2_percent if !missing(exp2_percent)
replace expall_percent=exp3_percent if !missing(exp3_percent)

count if !missing(exp1_percent) & !missing(exp1_amount)
local num_1_uas=r(N)
count if !missing(exp2_percent) & !missing(exp2_amount)
local num_2_uas=r(N)
count if !missing(exp3_percent) & !missing(exp3_amount)
local num_3_uas=r(N)

gen num_correct=0

save "uas_dataset",replace
