/**********************************************************************************************
* Purpose: Master do-file runs all do-files for "Do Thank-You Calls Increase Charitable Giving? 
Expert Forecasts and Field Experimental Evidence"
* Date modified: 2021 




BEFORE RUNNING: *Edit global dir to directory of master file if different than below

**********************************************************************************************/


*-------------------------------------------------------------------------------
*						DIRECTORY
*
*------------------------------------------------------------------------------- 

clear all 
set more off

*Enter directory of master file here
global dir "F:/replication-games/Replication package/thank_you_replication"

* Do-files directory
global dofiles "$dir/dofiles"

*===============================================================================
* A. CREATING DATASETS
*===============================================================================

do "$dofiles/01_dataprep/main.do" 

*===============================================================================
* B. CREATING MAIN FIGURES AND TABLES
*===============================================================================

do "$dofiles/02_analysis/main.do"


*MAIN FIGURES

*Figure 1 - Cumulative Probability of Donating in Experiment 

*Figure 2 - Forecasts of Treatment Effects (CAN'T BE CREATED WITHOUT UAS DATA)

*Figure 3 - Treatment Effect (in % Change):  Observed Versus Predicted

*MAIN TABLES

*Table 1 - Balance Table

*Table 2 - Treatment Effect



*===============================================================================
* C. CREATING APPENDIX FIGURES AND TABLES
*===============================================================================

do "$dofiles/03_appendix/main.do"


*APPENDIX FIGURES

*Figure A1 -  Flow Diagram: Experiments 1 and 3 created manually

*Figure A2 -  Distribution of Gift Amounts by Experiment

*Figure A3 -  Histograms of Forecasted Treatment Effects: Percent Donating (CAN'T BE CREATED WITHOUT UAS DATA)

*Figure A4 -  Histograms of Forecasted Treatment Effects: Amount|Donated (CAN'T BE CREATED WITHOUT UAS DATA)

*Figure A5 -  Characteristics of Charity Sample

*Figure A6 -  Observed and Predicted Donation Behavior, All Studies

*APPENDIX TABLES 

*Table A1 -   Factors Associated with Giving in the next Year

*Table A2 -   Average Gift Amount on Treatment, LATE Estimates

*Table A3 -  Treatment Effects in Future Years in Experiment 1

*Table A4 -  Effect of Treatment on Unconditional Gift Amount in Experiment 1

*Table A5 -  Average Overprediction by Experience and Charity Size

*Table A6 -  Robustness Test: Proportion Donating and Conditional Gift Card Amount in Experiment 1


	


