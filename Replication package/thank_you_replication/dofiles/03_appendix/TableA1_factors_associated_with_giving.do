********************************************************************************
*Purpose: This do file creates Table A1 
*Input Files: exp1_analysis_data, exp2_analysis_data, exp3_analysis_data
*Output Files: tableA1_factors_associated_with_giving.tex
********************************************************************************


	foreach experiment in 1 2 3 {
	clear
	cd "$dir/data"
	use exp`experiment'_analysis_data

		if `experiment'==2 {
			global controls 
			*reached var specific for LATE estimates
			gen reached = 1 if response_type == "Called: Contacted"
			replace reached=0 if response_type=="Called: Not Contacted" | response_type=="Control"
			}
		else {
			global controls female age_display2 age_display3 inc_display3/*
			*/ inc_display1 inc_display2 lor_display2 
			label var inc_display4 "Income below \\$34,000"
			label var inc_display3 "Income \\$35,000-\\$99,999"
			label var inc_display1 "Income \\$100,000-\\$174,999"
			label var inc_display2 "Income \\$175,000+"
			label var female "Female"
			label var age_display1 "18 to 44 years old"
			label var age_display2 "45 to 64 years old"
			label var age_display3 "65+ years old"
			label var lor_display1 "Residence length$<$5 years"
			label var lor_display2 "Residence length$>$5 years"
			
			*reached var specifc LATE estimates
			replace reached = 0 if treat==0
			}

			if `experiment'==3 {
				gen new_script=1 if exp3_treat==2
				replace new_script=0 if exp3_treat!=2
				label var new_script "New Call Script"
				}

label var treat "Call Treatment"
label var reached "Reached"

		*Table 3: OLS Regression
				if `experiment'==3 {
				*set station id for fixed effects
				xtset ii
				*donated Reg
				xtreg donated treat new_script payment_amount2 var12 $controls, fe
				estimates store exp`experiment'_reg_prob
				*cond. gift amount Reg
				xtreg gift_cond treat new_script payment_amount2 var12 $controls, fe
				estimates store exp`experiment'_reg_amount
				}

			else {
				if `experiment'==1 {
				xtset ii 
				*donated Reg
				xtreg donated treat payment_amount2 var12 $controls, fe
				estimates store exp`experiment'_reg_prob
				*cond. gift amount Reg
				xtreg gift_cond treat payment_amount2 var12 $controls, fe 
				estimates store exp`experiment'_reg_amount
				}
				else{
				* no fixed effets in exp 2
				*donated Reg
				reg donated treat payment_amount2 var12 $controls
				estimates store exp`experiment'_reg_prob
				*cond. gift amount Reg
				reg gift_cond treat payment_amount2 var12 $controls
				estimates store exp`experiment'_reg_amount
				}
				}
		
		
			if `experiment'==1 {
				*No Control Reg
				xtset ii
				xtreg `outcome' treat payment_amount2 var12, fe
				estimates store exp`experiment'_`outcome'_nocontrol
				*With Control Reg
				xtreg `outcome' treat payment_amount2 var12 $controls, fe
				estimates store exp`experiment'_`outcome'_control
				}
			if `experiment'==2 {
				*No Control Reg
				reg `outcome' treat payment_amount2 var12 
				estimates store exp`experiment'_`outcome'_nocontrol
				}
				}
	



cd "$dir/latex"

esttab exp1_reg_prob exp2_reg_prob exp3_reg_prob exp1_reg_amount exp2_reg_amount exp3_reg_amount/*
*/ using tableA1_factors_associated_with_giving.tex, nonotes cells(b(star fmt(2)) se(par))/*
*/ stats(r2 N, fmt(2 %15.0fc) label (R2 N)) label collabels(none) varlabels(_cons Constant)/*
*/ starlevels( * 0.10 ** 0.05 *** 0.010)  mgroups("\em{Donated}" "\em{Amount \$\mid\$ Donated}", pattern(1 0 0 1 0 0)/*
*/ prefix(\multicolumn{@span}{c}{) suffix(}) span erepeat(\cmidrule(lr){@span}))  /*            
*/ nonumbers mtitles("Experiment 1" "Experiment 2" "Experiment 3" "Experiment 1" "Experiment 2"  "Experiment 3")/* 
*/ order(treat new_script) replace


