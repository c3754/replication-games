********************************************************************************
*Purpose: This do file creates Figure A3
*Input Files: charity.dta, uas_dataset.dta
*Temp Files: exp1_hist_charityam.gph, exp2_hist_charityam.gph, exp3_hist_charityam.gph
*exp1_hist_uasam.gph, exp2_hist_uasam.gph, exp3_hist_uasam.gph
*Output Files: figureA4_all_hist_am.eps
********************************************************************************




cd "$dir/data"
use "charity.dta",clear



gen y=20
gen w=40
label var y "Treatment"

//--------------------------------------------------------
*Charity predicted vs actual gaps
//--------------------------------------------------------

foreach x of numlist 1/3 {
gen exp`x'_gapamount = exp`x'_amount - exp`x'_amountactual
gen exp`x'_gappercent= exp`x'_percent - exp`x'_percentactual
}
egen gap_amount= rowmean(exp*_gapamount)
egen gap_percent = rowmean(exp*_gappercent)

label var gap_amount "Overprediction (amount)"
label var gap_percent "Overprediction (percent)"



//--------------------------------------------------------
*histograms
//--------------------------------------------------------
replace y=0 in 5
replace w=0 in 5
cd "$dir/latex"
foreach x of numlist 1/3 {
summarize exp`x'_percentactual
gen exp`x'_conf_perchi=(r(mean) + .05*r(mean))
gen exp`x'_conf_perclow=(r(mean) - .05*r(mean))
summarize exp`x'_amountactual
gen exp`x'_conf_amhi=(r(mean) + .05*r(mean))
gen exp`x'_conf_amlow=(r(mean) - .05*r(mean))


capture erase exp`x'_hist_charityam.gph
twoway (histogram exp`x'_amount, fcolor(none) lcolor(gray) width(30) percent) (line w exp`x'_conf_amhi, /*
*/ lcolor(black) lpattern(dot)) (line w exp`x'_conf_amlow, lcolor(black) lpattern(dot))if exp`x'_amount!=1000, saving(exp`x'_hist_charityam,replace)/*
*/ graphregion(color(white)) legend(off) ylabel(,nogrid) title("Exp `x': Experts", size(medium))
}



cd "$dir/data"

use "uas_dataset",clear

*make histogram fragments for forecast histograms
gen y=20
gen w=40
replace y=0 in 788
replace w=0 in 788
label var y "Treatment"
cd "$dir/latex"

foreach x of numlist 1/3  {
label var exp`x'_percent "Experiment `x' (percent)"
label var exp`x'_amount "Experiment `x' (amount)"
replace num_correct=num_correct+1 if exp`x'_percent> (exp`x'_percentactual-(0.05*exp`x'_percentactual))/*
		*/ & exp`x'_percent<(exp`x'_percentactual+(0.05*exp`x'_percentactual))
	
*histograms
summarize exp`x'_percentactual
gen exp`x'_conf_perchi=(r(mean) + .05*r(mean))
gen exp`x'_conf_perclow=(r(mean) - .05*r(mean))
summarize exp`x'_amountactual
gen exp`x'_conf_amhi=(r(mean) + .05*r(mean))
gen exp`x'_conf_amlow=(r(mean) - .05*r(mean))
di "``x'_conf_perc'"
di "``x'_conf_am'"

capture erase exp`x'_hist_uasam.gph
twoway (histogram exp`x'_amount, fcolor(none) lcolor(gray) width(30) percent) (line w exp`x'_conf_amhi, /*
*/ lcolor(black) lpattern(dot)) (line w exp`x'_conf_amlow, lcolor(black) lpattern(dot)) if exp`x'_amount!=1000, saving(exp`x'_hist_uasam,replace)/*
*/ graphregion(color(white)) legend(off) ylabel(,nogrid) title("Exp. `x': Nonexperts", size(medium))
	}


**combining the graphs

graph combine exp1_hist_charityam.gph exp2_hist_charityam.gph exp3_hist_charityam.gph/*
*/ exp1_hist_uasam.gph exp2_hist_uasam.gph exp3_hist_uasam.gph,/*
*/ ycommon xcommon graphregion(color(white)) l1title("Percent of Sample", size(small)) b1title("Amount | Donated", size(small))
graph export figureA4_all_hist_am.eps, replace 


*erasing temp files

foreach x of numlist 1/3  {
capture erase exp`x'_hist_charityam.gph
capture erase exp`x'_hist_uasam.gph
}



