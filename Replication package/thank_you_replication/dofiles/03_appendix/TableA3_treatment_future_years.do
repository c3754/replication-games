*******************************************************************************
*Purpose: This do file creates Table A3
*Input Files: exp1_analysis_data
*Output Files: tableA3_treatment_future_years.tex
********************************************************************************



cd "$dir/data"
use exp1_analysis_data, clear

cd "$dir/latex"
file open fh using "tableA3_treatment_future_years.tex", write replace

		file write fh "\begin{subtable}[t]{\linewidth}" _n ///
			"\centering" _n ///
			"\vspace{0pt}" _n ///

foreach x of numlist 4/7 {
gen renewing`x'=0
replace renewing`x' =1 if var1`x'!=0 & var1`x'!=.
gen gift_cond`x'=payment_amount`x' if !missing(payment_amount`x') & payment_amount`x'>0
label var renewing`x' "Percent Donating"
label var payment_amount`x' "Amount Donated"
label var var1`x' "Number of Gifts"
label var gift_cond`x' "Amount \\$\mid\\$ Donated"
foreach var of varlist payment_amount`x' /*
*/ var1`x' renewing`x' gift_cond`x' {

if `var'==renewing`x' {
tabulate `var' treat, chi2
local `var'_p = r(p)
summarize `var' if treat==0
local `var'_cntrl_mean = r(mean)*100
local `var'_cntrl_se= (r(sd)/(sqrt(r(N))))*100
summarize `var' if treat==1
local `var'_treat_mean = r(mean)*100
local `var'_treat_se=(r(sd)/(sqrt(r(N))))*100
}
else {
ranksum `var', by(treat) porder
local `var'_p = 2 * normprob(-abs(r(z)))
summarize `var' if treat==0
local `var'_cntrl_mean = r(mean)
local `var'_cntrl_se= (r(sd)/(sqrt(r(N))))
summarize `var' if treat==1
local `var'_treat_mean = r(mean)
local `var'_treat_se=(r(sd)/(sqrt(r(N))))
}

*add significance stars
if ``var'_p'>.1 {
	local `var'_star=""
			}
if ``var'_p' <= .1 & ``var'_p' > .05 {
		local `var'_star="*"
		}
	if ``var'_p' <= .05 & ``var'_p' > .01 {
		local `var'_star="**"
		}
	if ``var'_p' <= .01 {
		local `var'_star="***"
		}
}
count if treat==1 & !missing(payment_amount`x')
local num_treat`x'=r(N)
count if treat==0 & !missing(payment_amount`x')
local num_control`x'=r(N)
}


foreach x of numlist 4/7 {
	local year=`x'-2
	file write fh "\caption{`year' Years Following Randomization}" _n ///
			"\vspace{0pt}" _n ///
			"\begin{tabular}{l c c c}" _n ///
			"& \multicolumn{1}{c}{Treatment} & \multicolumn{1}{c}{Control} & \multicolumn{1}{c}{p-value}\\" _n ///
			"\hline" _n ///
			"\hline" _n 
	foreach var of varlist renewing`x' gift_cond`x' {
				file write fh "\hspace{4mm}"`"`: var label `var''"'" & " %4.2f (``var'_treat_mean') " & " %4.2f (``var'_cntrl_mean') " & " %4.2f (``var'_p') "``var'_star' \\" _n 
				file write fh " &  (" %4.2f (``var'_treat_se') ") & (" %4.2f (``var'_cntrl_se') ") & \\" _n  
				}
	file write fh "\hline" _n
	file write fh "\hspace{4mm} N &  " %15.0fc (`num_treat`x'') " & " %15.0fc (`num_control`x'') " & \\" _n  
	file write fh "\hline" _n ///
				"\hline" _n 
	file write fh "\end{tabular}" _n
}

file write fh "\end{subtable}" _n
file close fh
