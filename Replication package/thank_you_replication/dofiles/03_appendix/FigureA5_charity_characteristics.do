********************************************************************************
*Purpose: This do file creates Figure A5
*Input Files: charity.dta
*Temp Files: num_donations_hist.gph, org_type_pie.gph
*Output Files: figureA5_charity_characteristics.pdf
********************************************************************************



cd "$dir/data"
use "charity.dta",clear

cd "$dir/latex"
histogram num_donations, discrete percent fcolor(none) lcolor(black) /*
*/ saving(num_donations_hist.gph,replace) xlabel(1 "100-999" 2 "1,000-9,999" 3 "10,000-99,999" 4 "100,000+")/*
*/ graphregion(color(white)) legend(off) xtitle("") ylabel(,nogrid)/*
*/ title("Number of Individual Donations per Year", size(medium))

graph pie, over(org_type) saving(org_type_pie.gph,replace) title("Charities by Field", size(medium))/*
*/ graphregion(color(white)) legend(order( 1 2 3 4 5 6 8 9 7) size(vsmall)) pie(_all, explode(0.25))

graph combine num_donations_hist.gph org_type_pie.gph/*
*/, iscale(0.8) graphregion(color(white)) xsize(9.0) 
graph export figureA5_charity_characteristics.pdf, replace

*erasing temp files

capture erase num_donations_hist.gph
capture erase org_type_pie.gph
