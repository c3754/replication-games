*******************************************************************************
*Purpose: This do file creates Table A5
*Input Files: charity.dta
*Output Files: tableA5_experience.tex
********************************************************************************


cd "$dir/data"
use "charity.dta",clear


//--------------------------------------------------------
*Charity predicted vs actual gaps
//--------------------------------------------------------

foreach x of numlist 1/3 {
gen exp`x'_gapamount = exp`x'_amount - exp`x'_amountactual
gen exp`x'_gappercent= exp`x'_percent - exp`x'_percentactual
}
egen gap_amount= rowmean(exp*_gapamount)
egen gap_percent = rowmean(exp*_gappercent)

label var gap_amount "Overprediction (amount)"
label var gap_percent "Overprediction (percent)"



//--------------------------------------------------------
*prediction by experience regression
//--------------------------------------------------------

foreach var of varlist highexp_org highexp_ind high_don does_calls {
 reg gap_amount `var'
		local t = _b[`var']/_se[`var']
		local `var'_p1 = 2*ttail(e(df_r),abs(`t'))
		local `var'_b1=_b[`var']
		local `var'_s1=_se[`var']
 reg gap_percent `var'
		local t = _b[`var']/_se[`var']
		local `var'_p2 = 2*ttail(e(df_r),abs(`t'))
		local `var'_b2=_b[`var']
		local `var'_s2=_se[`var']
		
	foreach y of numlist 1/2 {
	if ``var'_p`y'' > .1 local `var'_p`y'1 =""
	if ``var'_p`y'' > .05 & ``var'_p`y'' <=.1 local `var'_p`y'1="*"
	if ``var'_p`y'' > .01 & ``var'_p`y'' <=.05 local `var'_p`y'1="**"
	if ``var'_p`y'' <=.01 local `var'_p`y'1="***"
	}
	}

count if !missing(gap_amount)
local n1=r(N)
count if !missing(gap_percent)
local n2=r(N)
cd "$dir/latex"	
file open fh using "tableA5_experience.tex", write replace
		file write fh "\begin{tabular}{l*{7}{c}}" _n ///
			"\hline" _n ///
			"\hline" _n ///
			"& \multicolumn{1}{c}{Overprediction (\\$ per donation)} & \multicolumn{1}{c}{Overprediction (\% donated)} \\" _n ///
			"\hline" _n
foreach var of varlist highexp_org highexp_ind high_don does_calls {
		file write fh  "\hspace{4mm}"`"`: var label `var''"'" & " %4.2f (``var'_b1') "``var'_p11' & " %4.2f (``var'_b2') "``var'_p21' \\ " _n
		file write fh  "& (" %4.2f (``var'_s1') ") & (" %4.2f (``var'_s2') ")  \\ " _n
		}

file write fh "\hline" _n
file write fh  "\hspace{4mm} N & " %4.0f (`n1') " & " %4.0f (`n2') " \\ " _n
file write fh "\hline\hline" _n
file write fh "\end{tabular}" _n
file close fh
