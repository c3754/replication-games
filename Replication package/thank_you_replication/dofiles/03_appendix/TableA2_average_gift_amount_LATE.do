*******************************************************************************
*Purpose: This do file creates Table A2 
*Input Files: exp1_analysis_data, exp2_analysis_data, exp3_analysis_data
*Output Files: tableA2_average_gift_amount_LATE.tex
********************************************************************************


	foreach experiment in 1 2 3 {
	clear
	cd "$dir/data"
	use exp`experiment'_analysis_data

		if `experiment'==2 {
			global controls 
			*reached var specific for LATE estimates
			gen reached = 1 if response_type == "Called: Contacted"
			replace reached=0 if response_type=="Called: Not Contacted" | response_type=="Control"
			}
		else {
			global controls female age_display2 age_display3 inc_display3/*
			*/ inc_display1 inc_display2 lor_display2 
			label var inc_display4 "Income below \\$34,000"
			label var inc_display3 "Income \\$35,000-\\$99,999"
			label var inc_display1 "Income \\$100,000-\\$174,999"
			label var inc_display2 "Income \\$175,000+"
			label var female "Female"
			label var age_display1 "18 to 44 years old"
			label var age_display2 "45 to 64 years old"
			label var age_display3 "65+ years old"
			label var lor_display1 "Residence length$<$5 years"
			label var lor_display2 "Residence length$>$5 years"
			
			*reached var specifc LATE estimates
			replace reached = 0 if treat==0
			}

			if `experiment'==3 {
				gen new_script=1 if exp3_treat==2
				replace new_script=0 if exp3_treat!=2
				label var new_script "New Call Script"
				}

label var treat "Call Treatment"
label var reached "Reached"

		
		*Table 4: LATE, 2SLS Regression
	
			if `experiment'==3 {
			tab ii, gen(ii)
			drop ii 
			ivreg donated new_script payment_amount2 var12 ii* $controls (reached=treat)
			estimates store exp`experiment'_reg_prob

			ivreg gift_cond new_script payment_amount2 var12 $controls ii* (reached=treat)
			estimates store exp`experiment'_reg_amount
			}
			
			if `experiment'==1 {
			tab ii, gen(ii)
			drop ii 
			ivreg donated payment_amount2 var12 ii* $controls (reached=treat)
			estimates store exp`experiment'_reg_prob

			ivreg gift_cond payment_amount2 var12 $controls ii* (reached=treat)
			estimates store exp`experiment'_reg_amount
			}

			if `experiment'==2 {
			ivreg donated payment_amount2 var12 $controls (reached=treat)
			estimates store exp`experiment'_reg_prob

			ivreg gift_cond payment_amount2 var12 $controls (reached=treat)
			estimates store exp`experiment'_reg_amount
			}
			
	}
	

cd "$dir/latex"


esttab exp1_reg_prob exp2_reg_prob exp3_reg_prob exp1_reg_amount exp2_reg_amount exp3_reg_amount/*
*/ using tableA2_average_gift_amount_LATE.tex, nonotes cells(b(star fmt(2)) se(par))/*
*/ stats(r2 N, fmt(2 %15.0fc) label (R2 N)) label collabels(none) varlabels(_cons Constant)/*
*/ starlevels( * 0.10 ** 0.05 *** 0.010) drop(ii*) mgroups("\em{Donated}" "\em{Amount \$\mid\$ Donated}", pattern(1 0 0 1 0 0)/*
*/ prefix(\multicolumn{@span}{c}{) suffix(}) span erepeat(\cmidrule(lr){@span}))/*
*/ nonumbers mtitles("Experiment 1" "Experiment 2" "Experiment 3" "Experiment 1" "Experiment 2" "Experiment 3")/* 
*/ order(reached new_script) replace

