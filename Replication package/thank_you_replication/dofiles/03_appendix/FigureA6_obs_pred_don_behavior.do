********************************************************************************
*Purpose: This do file creates Figure A6
*Input Files: charity_round2.dta

*Temp Files:
*In .gph and .png form
*gifts_1, gifts_2, calling_1, calling_2, agency_1,agency_2

*Output Files:
*figureA6_agency.png, figureA6_calling.png, figureA6_gifts.png, figureA6_info_1.png, figureA6_door_1.png
********************************************************************************


cd "$dir/data"
use "charity_round2", clear


**creating globals 

global donation_rates mail_1 mail_2 door_1 door_2 door_3 matching_1 matching_2 matching_3 volunteer_1 volunteer_2 agency_1 agency_3
global cond_amounts mail_3 mail_4 volunteer_3 volunteer_4 calling_1 calling_2 calling_3 matching_4 matching_5 matching_6 agency_2 agency_4


foreach var of varlist $cond_amounts $donation_rates {
	gen peffect_`var'=`var' - `var'_control
	gen aeffect_`var'=`var'_actual - `var'_control
	gen sepeffect_`var'=`var' - `var'_control
}

*gen se for actual obsv, donation rates
foreach var of varlist $donation_rates {
summarize `var'_actual
local success1=r(mean)/100
summarize `var'_control
local success2=r(mean)/100
di `success1'
di `success2'
di (`success1'*(1-`success1'))*(`success2'^2)
gen seaeffect_`var'=sqrt(((`success1'*(1-`success1'))/`var'_nactual) + ((`success2'*(1-`success2'))/`var'_ncontrol))
replace seaeffect_`var'=seaeffect_`var'*100

gen setreatment_`var'=(sqrt((`success1'*(1-`success1'))/`var'_nactual))*100
gen secontrol_`var'=(sqrt((`success2'*(1-`success2'))/`var'_ncontrol))*100
}



*gen se for actual obsv, cond amounts
foreach var of varlist $cond_amounts {
gen seaeffect_`var'=sqrt((`var'_seactual)^2 + (`var'_secontrol)^2)
gen setreatment_`var'=`var'_seactual
gen secontrol_`var'=`var'_secontrol
}

count if !missing(peffect_mail_1) | !missing(finished)
local obsv=r(N)

foreach var of varlist $donation_rates $cond_amounts {
	gen treatment_`var'=`var'_actual
	gen control_`var'=`var'_control
	gen forecast_`var'=`var'
	gen seforecast_`var'=forecast_`var'
}


collapse (mean) aeffect_* peffect_* seaeffect_* treatment_* control_* forecast_* setreatment_* secontrol_* /*
*/ *_confidence (semean) sepeffect_* seforecast_*


local count=1
foreach var in $donation_rates $cond_amounts {
	rename peffect_`var' peffect`count'
	rename sepeffect_`var' sepeffect`count'
	rename aeffect_`var' aeffect`count'
	rename seaeffect_`var' seaeffect`count'
	rename treatment_`var' treatment`count'
	rename control_`var' control`count'
	rename forecast_`var' forecast`count'
	rename setreatment_`var' setreatment`count'
	rename secontrol_`var' secontrol`count'
	rename seforecast_`var' seforecast`count'
	local count=`count'+1
}

gen id=1
reshape long peffect aeffect sepeffect seaeffect treatment control forecast/*
*/ setreatment secontrol seforecast, j(experiment) i(id)
drop id

	label define experiment 1 "Gifts 1" 2 "Gifts 2" 3 "Opt out 1" 4 "Opt out 2" 5 "Opt out 3" /*
	*/ 6 "Matching 1" 7 "Matching 2" 8 "Matching 3" 9 "Paid info" 10 "Volunteer info" 11 "Agency 1" 12 "Agency 2"/*
	*/ 13 "Gifts 1" 14 "Gifts 2" 15 "Paid info" 16 "Volunteer info" 17 "Info 1" 18 "Info 2" 19 "Info 3"/*
	*/ 20 "Matching 1" 21 "Matching 2" 22 "Matching 3" 23 "Agency 1" 24 "Agency 2"
	
	label values experiment experiment


gen hi_p = peffect + 1.96*sepeffect
gen lo_p = peffect - 1.96*sepeffect
gen hi_a = aeffect + 1.96*seaeffect
gen lo_a = aeffect - 1.96*seaeffect



//----------------------
* Mail Graph
//--------------------
gen control_location=1 if inlist(experiment, 1, 13, 9,15,17,6,20,3,11,23)
replace control_location=4 if inlist(experiment, 10, 16,4,12,24)
replace control_location=7 if inlist(experiment, 5)
gen treat_location=2 if inlist(experiment, 1, 13, 9,15,17,6,20,3,11,22,23)
replace treat_location=3 if inlist(experiment, 2, 14)
replace treat_location=5 if inlist(experiment,10,16,4,12,24)
replace treat_location=4 if inlist(experiment,18,7,21)
replace treat_location=6 if inlist(experiment,19,8,22)
replace treat_location=8 if inlist(experiment,5)
gen forecast_location=4 if inlist(experiment, 1, 13)
replace forecast_location=5 if inlist(experiment, 2, 14,18,7,21)
replace forecast_location=3 if inlist(experiment,9,15,17,6,20,3,11,23)
replace forecast_location=6 if inlist(experiment,10,16,4,12,24)
replace forecast_location=7 if inlist(experiment,19,8,22)
replace forecast_location=9 if inlist(experiment,5)

foreach type in control treatment forecast {
gen hi_`type'=`type'+1.96*se`type'
gen lo_`type'=`type'-1.96*se`type'
}



cd "$dir/latex"


*Mailing Postcard gifts

twoway (bar control control_location if experiment==1, fcolor(midblue) lcolor(black) barwidth(.9)) (bar treatment treat_location if experiment==1, barwidth(.9) fcolor(cranberry) lcolor(black))/*
*/ (bar treatment treat_location if experiment==2, fcolor(green) lcolor(black) barwidth(.9)) (bar forecast forecast_location if experiment==1, fcolor(cranberry*.5) lcolor(black) barwidth(.9)) /*
*/ (bar forecast forecast_location if experiment==2, fcolor(green*.5) lcolor(black) barwidth(.9)) (rcap hi_control lo_control control_location if experiment==1, lcolor(red))/*
*/ (rcap hi_treatment lo_treatment treat_location if experiment==1 | experiment==2, lcolor(red)) /*
*/ (rcap hi_forecast lo_forecast forecast_location if experiment==1 | experiment==2, lcolor(red)), graphregion(color(white)) ylabel(0(10)40,nogrid) ytitle("Proportion Donating (%)") /*
*/ legend(off) xlabel(1 "control"  2 `""treatment" "1 postcard""' 3 `""treatment" "4 postcards""' 4 `""forecast" "1 postcard""' 5 `""forecast" "4 postcards""', labsize(small)) saving(gifts_1.gph,replace)
graph export gifts_1.png, replace 


twoway (bar control control_location if experiment==13, fcolor(midblue) lcolor(black) barwidth(.9)) (bar treatment treat_location if experiment==13, fcolor(cranberry) lcolor(black) barwidth(.9))/*
*/ (bar treatment treat_location if experiment==14, barwidth(.9) fcolor(green) lcolor(black)) (bar forecast forecast_location if experiment==13, barwidth(.9) fcolor(cranberry*.5) lcolor(black)) /*
*/ (bar forecast forecast_location if experiment==14, fcolor(green*.5) lcolor(black) barwidth(.9)) /*
*/ (rcap hi_control lo_control control_location if experiment==13, lcolor(red))/*
*/ (rcap hi_treatment lo_treatment treat_location if experiment==13 | experiment==14, lcolor(red)) /*
*/ (rcap hi_forecast lo_forecast forecast_location if experiment==13 | experiment==14, lcolor(red)), graphregion(color(white)) ylabel(,nogrid) ytitle("Giving Amount ($)") /*
*/ legend(off) ylabel(50 (10) 80) yscale(range( 50 80)) xlabel(1 "control"  2 `""treatment" "1 postcard""' 3 `""treatment" "4 postcards""' 4 `""forecast" "1 postcard""' 5 `""forecast" "4 postcards""', labsize(small)) saving(gifts_2.gph,replace)
graph export gifts_2.png, replace 




*Calling graphs
twoway (bar control control_location if experiment==6, fcolor(midblue) lcolor(black) barwidth(.9) )/*
*/ (bar treatment treat_location if experiment==6 | experiment==7 | experiment==8, fcolor(cranberry) lcolor(black) barwidth(.9))/*
*/ (bar forecast forecast_location if experiment==6 | experiment==7 | experiment==8, fcolor(cranberry*.5) lcolor(black) barwidth(.9)) /*
*/ (rcap hi_control lo_control control_location if experiment==6 | experiment==7 | experiment==8, lcolor(red))/*
*/ (rcap hi_treatment lo_treatment treat_location if experiment==6 | experiment==7 | experiment==8, lcolor(red)) /*
*/ (rcap hi_forecast lo_forecast forecast_location if experiment==6 | experiment==7 | experiment==8, lcolor(red)), graphregion(color(white)) ylabel(,nogrid) ytitle("Proportion Donating (%)")  /*
*/ legend(off) xlabel(1 "control" 2 `""treatment" "1:1""' 3 `""forecast" "1:1""'/*
*/ 4 `""treatment" "2:1""' 5 `""forecast" "2:1""' 6 `""treatment" "3:1""' 7 `""forecast" "3:1""', labsize(small))  saving(calling_1.gph,replace)
graph export calling_1.png, replace 

twoway (bar control control_location if experiment==20, fcolor(midblue) lcolor(black) barwidth(.9) )/*
*/ (bar treatment treat_location if experiment==20 | experiment==21 | experiment==22, fcolor(cranberry) lcolor(black) barwidth(.9))/*
*/ (bar forecast forecast_location if experiment==20 | experiment==21 | experiment==22, fcolor(cranberry*.5) lcolor(black) barwidth(.9)) /*
*/ (rcap hi_control lo_control control_location if experiment==20 | experiment==21 | experiment==22, lcolor(red))/*
*/ (rcap hi_treatment lo_treatment treat_location if experiment==20 | experiment==21 | experiment==22, lcolor(red)) /*
*/ (rcap hi_forecast lo_forecast forecast_location if experiment==20 | experiment==21 | experiment==22, lcolor(red)), graphregion(color(white)) ylabel(,nogrid) ytitle("Giving Amount ($)")  /*
*/ legend(off) xlabel(1 "control" 2 `""treatment" "1:1""' 3 `""forecast" "1:1""'/*
*/ 4 `""treatment" "2:1""' 5 `""forecast" "2:1""' 6 `""treatment" "3:1""' 7 `""forecast" "3:1""', labsize(small)) saving(calling_2.gph,replace)
graph export calling_2.png, replace 



*Agency graphs
twoway (bar control control_location if experiment==11 | experiment==12, fcolor(midblue) lcolor(black) barwidth(.9))/*
*/ (bar treatment treat_location if experiment==11 | experiment==12, fcolor(cranberry) lcolor(black) barwidth(.9))/*
*/ (bar forecast forecast_location if experiment==11 | experiment==12, fcolor(cranberry*.5) lcolor(black) barwidth(.9)) /*
*/ (rcap hi_control lo_control control_location if experiment==11 | experiment==12, lcolor(red))/*
*/ (rcap hi_treatment lo_treatment treat_location if experiment==11 | experiment==12, lcolor(red)) /*
*/ (rcap hi_forecast lo_forecast forecast_location if experiment==11 | experiment==12, lcolor(red)), graphregion(color(white)) ylabel(,nogrid) ytitle("Proportion Donating (%)")  /*
*/ legend(off) xlabel(1 "control" 2 `""treatment" "overall""' 3 "forecast" 4 "control" 5 `""treatment" "top 5%""'/*
*/ 6 "forecast") saving(agency_1.gph,replace)
graph export agency_1.png, replace 

twoway (bar control control_location if experiment==23 | experiment==24, fcolor(midblue) lcolor(black) barwidth(.9)) /*
*/ (bar treatment treat_location if experiment==23 | experiment==24, fcolor(cranberry) lcolor(black) barwidth(.9))/*
*/(bar forecast forecast_location if experiment==23 | experiment==24, fcolor(cranberry*.5) lcolor(black) barwidth(.9)) /*
*/ (rcap hi_control lo_control control_location if experiment==23 | experiment==24, lcolor(red))/*
*/ (rcap hi_treatment lo_treatment treat_location if experiment==23 | experiment==24, lcolor(red)) /*
*/ (rcap hi_forecast lo_forecast forecast_location if experiment==23 | experiment==24, lcolor(red)), /*
*/ graphregion(color(white)) ytitle("Giving amount ($)") ylabel(,nogrid) /*
*/ legend(off) xlabel(1 "control" 2 `""treatment" "overall""' 3 "forecast" 4 "control" 5 `""treatment" "top 5%""'/*
*/ 6 "forecast")  saving(agency_2.gph,replace) 
graph export agency_2.png, replace 

*Combining
foreach x in agency calling gifts  {
graph combine `x'_1.gph `x'_2.gph, graphregion(color(white)) xsize(7)
graph export figureA6_`x'_ppt.png, replace
}


*Calling in (info)
twoway (bar control control_location if experiment==17, fcolor(midblue) barwidth(.9) lcolor(black) )/*
*/ (bar treatment treat_location if experiment==17 | experiment==18 | experiment==19, fcolor(cranberry) barwidth(.9) lcolor(black))/*
*/ (bar forecast forecast_location if experiment==17 | experiment==18 | experiment==19, fcolor(cranberry*.5) barwidth(.9) lcolor(black)) /*
*/ (rcap hi_control lo_control control_location if experiment==17 | experiment==18 | experiment==19, lcolor(red))/*
*/ (rcap hi_treatment lo_treatment treat_location if experiment==17 | experiment==18 | experiment==19, lcolor(red)) /*
*/ (rcap hi_forecast lo_forecast forecast_location if experiment==17 | experiment==18 | experiment==19, lcolor(red)), graphregion(color(white)) ylabel(,nogrid) ytitle("Giving Amount ($)")  /*
*/ legend(off) xlabel(1 "control" 2 `""treatment" "$75""' 3 `""forecast" "$75""'/*
*/ 4 `""treatment" "$180""' 5 `""forecast" "$180""' 6 `""treatment" "$300""' 7 `""forecast" "$300""')
graph export figureA6_info_1.png, replace 



*Door to door graphs
twoway (bar control control_location if experiment==3 | experiment==4 | experiment==5, fcolor(midblue) lcolor(black) barwidth(.9) )/*
*/ (bar treatment treat_location if experiment==3 | experiment==4 | experiment==5, fcolor(cranberry) lcolor(black) barwidth(.9))/*
*/ (bar forecast forecast_location if experiment==3 | experiment==4 | experiment==5, fcolor(cranberry*.5) lcolor(black) barwidth(.9)) /*
*/ (rcap hi_control lo_control control_location if experiment==3 | experiment==4 | experiment==5, lcolor(red))/*
*/ (rcap hi_treatment lo_treatment treat_location if experiment==3 | experiment==4 | experiment==5, lcolor(red)) /*
*/ (rcap hi_forecast lo_forecast forecast_location if experiment==3 | experiment==4 | experiment==5, lcolor(red)), graphregion(color(white)) ylabel(,nogrid) ytitle("Proportion (%)") /*
*/ legend(off) xlabel(1 "control" 2 `""treat" "door opening""' 3 "forecast" 4 "control" 5 `""treat" "donating""' 6 "forecast" /*
*/ 7 "control" 8 `""treat" "small donor""' 9 "forecast") 
graph export figureA6_door_1.png, replace 


*erasing temp files 

foreach var in .png .gph{
cap erase gifts_1`var'
cap erase gifts_2`var'
cap erase calling_1`var'
cap erase calling_2`var'
cap erase agency_1`var'
cap erase agency_2`var'
}
