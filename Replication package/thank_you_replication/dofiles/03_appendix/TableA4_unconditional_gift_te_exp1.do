*******************************************************************************
*Purpose: This do file creates Table A4
*Input Files: exp1_analysis_data
*Output Files: tableA4_unconditional_gift_te_exp1.tex
********************************************************************************


cd "$dir/data"
clear
use exp1_analysis_data

global controls female age_display2 age_display3 inc_display3/*
*/ inc_display1 inc_display2 lor_display2 
label var inc_display4 "Income below \\$34,000"
label var inc_display3 "Income \\$35,000-\\$99,999"
label var inc_display1 "Income \\$100,000-\\$174,999"
label var inc_display2 "Income \\$175,000+"
label var female "Female"
label var age_display1 "18 to 44 years old"
label var age_display2 "45 to 64 years old"
label var age_display3 "65+ years old"
label var lor_display1 "Residence length$<$5 years"
label var lor_display2 "Residence length$>$5 years"
label var treat "Call Treatment"

xtset ii 
				
xtreg payment_amount3 treat payment_amount2 var12 $controls , fe
estimates store exp1_reg_prob
									
cd "$dir/latex"
	
esttab exp1_reg_prob /*
*/ using tableA4_unconditional_gift_te_exp1.tex, nonotes cells(b(star fmt(2)) se(par))/*
*/ stats(r2 N, fmt(2 %15.0fc) label (R2 N)) label collabels(none) varlabels(_cons Constant)/*
*/ starlevels( * 0.10 ** 0.05 *** 0.010)/*            
*/ nonumbers mtitles("Unconditional Gift Amount")/* 
*/ replace		
