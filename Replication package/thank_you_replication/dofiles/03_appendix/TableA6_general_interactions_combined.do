*******************************************************************************
*Purpose: This do file creates Table A6
*Input Files: exp1_analysis_data.dta
*Output Files: tableA6_general_interactions_combined.tex
********************************************************************************



cd "$dir/data"

use exp1_analysis_data, clear

replace pledge2=1 if pledge2>0 & !missing(pledge2)
*replace category_p2=1 if category_p2>0 & !missing(category_p2)

*make interaction terms
foreach x of numlist 1/3 {
gen treat_age`x'=age_display`x'*treat
}
foreach x of numlist 1/4 {
gen treat_inc`x'=inc_display`x'*treat
}
gen treat_female=female*treat
gen treat_payment_amount2=payment_amount2*treat
gen treat_var12=var12*treat
gen treat_p=pledge2*treat
gen treat_residence=treat*lor_display2
label var gift_cond "Exp. 1" 
label var donated "Exp. 1" 
label var treat_female "Treatment*Female"
label var treat_payment_amount2 "Treatment*Baseline Donation"
label var treat_var12 "Treatment*Baseline Gifts"
label var treat_p "Treatment*Pledge Drive"
label var treat_age2 "Treatment*45-64 years old"
label var treat_age3 "Treatment*65+ years old"
label var treat_inc1 "Treatment*\\$100k-175k+"
label var treat_inc2 "Treatment*\\$175k+"
label var treat_inc3 "Treatment*\\$35k-99k"
label var treat_residence "Treatment*Residence length 5+ years"
label var treat "Treatment"
label var pledge2 "Pledge Drive"
label var donated "Prop. Donating"
label var gift_cond "Amount Donated"

xtset ii
cd "$dir/latex"
eststo clear
eststo: quietly xtreg donated treat pledge2 treat_p , fe
eststo: quietly xtreg gift_cond treat pledge2 treat_p , fe
esttab using tableA6_general_interactions_combined.tex, keep(treat pledge2 treat_p) cells(b(star fmt(2)) se(par))/* 
  */ stats(r2 N, fmt(2 %15.0fc) label(R2 N))   /*
  */ label collabels(none) /*
  */ star(* 0.10 ** 0.05 *** 0.01) nonotes replace
