/***********************************************************************************************
*This dofile can only be run after the running all dofiles located in the dofiles/dataprep folder
* Purpose: Runs all do-files located in the dofiles/appendix folder
* Date modified: 2021 

*Instructions: Uncomment lines 27 and 36 after data from UAS has been received and all do files
in the dataprep folder have been run
**********************************************************************************************/

*===============================================================================
* CREATING APPENDIX FIGURES AND TABLES
*===============================================================================

*APPENDIX FIGURES

*Figure A1 -  Flow Diagram: Experiments 1 and 3 created manually

*Figure A2 -  Distribution of Gift Amounts by Experiment
do "$dofiles/03_appendix/FigureA2_kdensity.do" 

*Figure A3 -  Histograms of Forecasted Treatment Effects: Percent Donating

/***********************************************************************************************
Uncomment below line after data from UAS has been received and all do files in the dataprep folder
have been run
***********************************************************************************************/
do "$dofiles/03_appendix/FigureA3_historgram_forecasted.do" 

*Figure A4 -  Histograms of Forecasted Treatment Effects: Amount|Donated

/***********************************************************************************************
Uncomment below line after data from UAS has been received and all do files in the dataprep folder
have been run
***********************************************************************************************/

do "$dofiles/03_appendix/FigureA4_histogram_forecasted_conditional.do" 

*Figure A5 -  Characteristics of Charity Sample
do "$dofiles/03_appendix/FigureA5_charity_characteristics.do" 

*Figure A6 -  Observed and Predicted Donation Behavior, All Studies
do "$dofiles/03_appendix/FigureA6_obs_pred_don_behavior.do" 

*APPENDIX TABLES 

*Table A1 -   Factors Associated with Giving in the next Year
do "$dofiles/03_appendix/TableA1_factors_associated_with_giving.do" 

*Table A2 -   Average Gift Amount on Treatment, LATE Estimates
do "$dofiles/03_appendix/TableA2_average_gift_amount_LATE.do" 

*Table A3 -  Treatment Effects in Future Years in Experiment 1
do "$dofiles/03_appendix/TableA3_treatment_future_years.do" 

*Table A4 -  Effect of Treatment on Unconditional Gift Amount in Experiment 1
do "$dofiles/03_appendix/TableA4_unconditional_gift_te_exp1.do"

*Table A5 -  Average Overprediction by Experience and Charity Size
do "$dofiles/03_appendix/TableA5_experience.do" 

*Table A6 -  Robustness Test: Proportion Donating and ConditionalGift Card Amount in Experiment 1
do "$dofiles/03_appendix/TableA6_general_interactions_combined.do"


	
