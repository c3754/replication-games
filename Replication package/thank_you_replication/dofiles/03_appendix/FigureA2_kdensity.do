********************************************************************************
*Purpose: This do file creates Figure A2
*Input Files: exp1_analysis_data.dta, exp2_analysis_data.dta, exp3_analysis_data.dta
*Output Files: figureA2_kdensity_exp1.png, figureA2_kdensity_exp2.png, figureA2_kdensity_exp3.png
********************************************************************************


foreach x of numlist 1/3 {
cd "$dir/data"
use exp`x'_analysis_data, clear

if `x'==1 | `x'==2 {


twoway (kdensity payment_amount3 if treat==1 & payment_amount3>0 & payment_amount3<500, bwidth(3) lcolor(red))/*
*/ (kdensity payment_amount3 if treat==0 & payment_amount3>0 & payment_amount3<500, bwidth(3) lcolor(blue) lpattern(dash))/*
*/ , xlabel(0 (50) 500) legend(order (1 "Treatment" 2 "Control")) xtitle("Amount | Donated") ytitle("Kernel Density") graphregion(color(white))
}
else {

twoway (kdensity payment_amount3 if treat==1 & script==1 & payment_amount3>0 & payment_amount3<500, bwidth(3) lcolor(red))/*
*/ (kdensity payment_amount3 if treat==1 & script==0 & payment_amount3>0 & payment_amount3<500, bwidth(3) lcolor(green) lpattern(dash) )/*
*/ (kdensity payment_amount3 if treat==0 & script==0 & payment_amount3>0 & payment_amount3<500, bwidth(3) lcolor(blue) lpattern(dash))/*
*/, legend(order (1 "New Script" 2 "Old Script" 3 "Control"))  xlabel(0 (50) 500) xtitle("Amount | Donated") ytitle("Kernel Density") graphregion(color(white))/*
*/
}
graph export "$dir/latex/figureA2_kdensity_exp`x'.png", replace
}
